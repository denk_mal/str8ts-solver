# a small str8ts solver

https://www.str8ts.com

* autorun (ctrl-click on 'Take Step' button) with variable speed
* singlestep/speedstep (shift-click on 'Take Step' button)\
  singlestep: stop on every step\
  speedstep: stops on next successful step
* solver includes fish and setti solutions
* 'guess best' button;\
   first click -> search for best guess\
   second click -> insert best guess found
* support of game files\
  line contains \<name\> \<fielddescription\>\
  fielddescription contains 182 chars like in the url for the original solver\
  e.g.\
  Extreme Str8ts #100 001000000000000900...000000000001000001
* simple editor
* simple save adds current gamefield to the current gamefile
