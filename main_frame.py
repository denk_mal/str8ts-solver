# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6-dirty)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Str8ts Solver", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		b_sizer0 = wx.BoxSizer( wx.VERTICAL )

		b_sizer_toolbar = wx.BoxSizer( wx.HORIZONTAL )

		self.m_panel_toolbar = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.BORDER_RAISED|wx.TAB_TRAVERSAL )
		b_sizer01 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_create_new = wx.BitmapButton( self.m_panel_toolbar, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_create_new.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_REPORT_VIEW,  ) )
		b_sizer01.Add( self.m_create_new, 0, wx.ALL, 5 )

		self.m_savegame = wx.BitmapButton( self.m_panel_toolbar, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_savegame.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_FILE_SAVE_AS,  ) )
		b_sizer01.Add( self.m_savegame, 0, wx.ALL, 5 )


		b_sizer01.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_batchmode = wx.BitmapToggleButton( self.m_panel_toolbar, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_EXECUTABLE_FILE,  ), wx.DefaultPosition, wx.DefaultSize, 0 )

		self.m_batchmode.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_EXECUTABLE_FILE,  ) )
		b_sizer01.Add( self.m_batchmode, 0, wx.ALL, 5 )


		b_sizer01.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_statictext01 = wx.StaticText( self.m_panel_toolbar, wx.ID_ANY, u"Select Game:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext01.Wrap( -1 )

		b_sizer01.Add( self.m_statictext01, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		m_select_gameChoices = []
		self.m_select_game = wx.Choice( self.m_panel_toolbar, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_select_gameChoices, 0 )
		self.m_select_game.SetSelection( 0 )
		b_sizer01.Add( self.m_select_game, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticline1 = wx.StaticLine( self.m_panel_toolbar, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		b_sizer01.Add( self.m_staticline1, 0, wx.EXPAND |wx.ALL, 5 )

		self.m_guess = wx.BitmapButton( self.m_panel_toolbar, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_guess.SetBitmap( wx.ArtProvider.GetBitmap( u"gtk-redo",  ) )
		self.m_guess.SetHelpText( u"Guess best" )

		b_sizer01.Add( self.m_guess, 0, wx.ALL, 5 )

		self.m_staticline2 = wx.StaticLine( self.m_panel_toolbar, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		b_sizer01.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

		self.m_settings = wx.BitmapButton( self.m_panel_toolbar, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_settings.SetBitmap( wx.ArtProvider.GetBitmap( u"gtk-preferences",  ) )
		self.m_settings.SetBitmapPressed( wx.NullBitmap )
		b_sizer01.Add( self.m_settings, 0, wx.ALL, 5 )


		self.m_panel_toolbar.SetSizer( b_sizer01 )
		self.m_panel_toolbar.Layout()
		b_sizer01.Fit( self.m_panel_toolbar )
		b_sizer_toolbar.Add( self.m_panel_toolbar, 1, wx.EXPAND, 5 )


		b_sizer0.Add( b_sizer_toolbar, 0, wx.EXPAND, 5 )

		b_sizer1 = wx.BoxSizer( wx.HORIZONTAL )

		b_sizer11 = wx.BoxSizer( wx.VERTICAL )

		fg_sizer111 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fg_sizer111.SetFlexibleDirection( wx.BOTH )
		fg_sizer111.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )


		fg_sizer111.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		b_sizer1111 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext11111 = wx.StaticText( self, wx.ID_ANY, u"1", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11111.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11111, 1, wx.ALL, 5 )

		self.m_statictext11112 = wx.StaticText( self, wx.ID_ANY, u"2", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11112.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11112, 1, wx.ALL, 5 )

		self.m_statictext11113 = wx.StaticText( self, wx.ID_ANY, u"3", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11113.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11113, 1, wx.ALL, 5 )

		self.m_statictext11114 = wx.StaticText( self, wx.ID_ANY, u"4", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11114.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11114, 1, wx.ALL, 5 )

		self.m_statictext11115 = wx.StaticText( self, wx.ID_ANY, u"5", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11115.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11115, 1, wx.ALL, 5 )

		self.m_statictext11116 = wx.StaticText( self, wx.ID_ANY, u"6", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11116.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11116, 1, wx.ALL, 5 )

		self.m_statictext11117 = wx.StaticText( self, wx.ID_ANY, u"7", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11117.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11117, 1, wx.ALL, 5 )

		self.m_statictext11118 = wx.StaticText( self, wx.ID_ANY, u"8", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11118.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11118, 1, wx.ALL, 5 )

		self.m_statictext11119 = wx.StaticText( self, wx.ID_ANY, u"9", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11119.Wrap( -1 )

		b_sizer1111.Add( self.m_statictext11119, 1, wx.ALL, 5 )


		fg_sizer111.Add( b_sizer1111, 1, wx.EXPAND, 5 )

		b_sizer1112 = wx.BoxSizer( wx.VERTICAL )

		b_sizer11121 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111211 = wx.StaticText( self, wx.ID_ANY, u"A", wx.Point( -1,-1 ), wx.DefaultSize, 0 )
		self.m_statictext111211.Wrap( -1 )

		b_sizer11121.Add( self.m_statictext111211, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11121, 1, wx.EXPAND, 5 )

		b_sizer11122 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111221 = wx.StaticText( self, wx.ID_ANY, u"B", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111221.Wrap( -1 )

		b_sizer11122.Add( self.m_statictext111221, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11122, 1, wx.EXPAND, 5 )

		b_sizer11123 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111231 = wx.StaticText( self, wx.ID_ANY, u"C", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111231.Wrap( -1 )

		b_sizer11123.Add( self.m_statictext111231, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11123, 1, wx.EXPAND, 5 )

		b_sizer11124 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111241 = wx.StaticText( self, wx.ID_ANY, u"D", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111241.Wrap( -1 )

		b_sizer11124.Add( self.m_statictext111241, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11124, 1, wx.EXPAND, 5 )

		b_sizer11125 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111251 = wx.StaticText( self, wx.ID_ANY, u"E", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111251.Wrap( -1 )

		b_sizer11125.Add( self.m_statictext111251, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11125, 1, wx.EXPAND, 5 )

		b_sizer11126 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111261 = wx.StaticText( self, wx.ID_ANY, u"F", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111261.Wrap( -1 )

		b_sizer11126.Add( self.m_statictext111261, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11126, 1, wx.EXPAND, 5 )

		b_sizer11127 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111271 = wx.StaticText( self, wx.ID_ANY, u"G", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111271.Wrap( -1 )

		b_sizer11127.Add( self.m_statictext111271, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11127, 1, wx.EXPAND, 5 )

		b_sizer11128 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111281 = wx.StaticText( self, wx.ID_ANY, u"H", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111281.Wrap( -1 )

		b_sizer11128.Add( self.m_statictext111281, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11128, 1, wx.EXPAND, 5 )

		b_sizer11129 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111291 = wx.StaticText( self, wx.ID_ANY, u"J", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext111291.Wrap( -1 )

		b_sizer11129.Add( self.m_statictext111291, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer1112.Add( b_sizer11129, 1, wx.EXPAND, 5 )


		fg_sizer111.Add( b_sizer1112, 1, wx.EXPAND, 5 )

		self.m_gameboard = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 540,540 ), wx.TAB_TRAVERSAL )
		self.m_gameboard.SetBackgroundColour( wx.Colour( 0, 0, 0 ) )

		g_sizer1113 = wx.GridSizer( 9, 9, 0, 0 )

		g_sizer1113.SetMinSize( wx.Size( 540,540 ) )

		self.m_gameboard.SetSizer( g_sizer1113 )
		self.m_gameboard.Layout()
		fg_sizer111.Add( self.m_gameboard, 0, 0, 0 )


		b_sizer11.Add( fg_sizer111, 1, wx.EXPAND, 5 )

		b_sizer112 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext1121 = wx.StaticText( self, wx.ID_ANY, u"Highest Solution Step:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext1121.Wrap( -1 )

		b_sizer112.Add( self.m_statictext1121, 0, wx.ALL, 5 )

		self.m_resultstep = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_resultstep.Wrap( -1 )

		self.m_resultstep.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )
		self.m_resultstep.SetForegroundColour( wx.Colour( 0, 192, 0 ) )

		b_sizer112.Add( self.m_resultstep, 1, wx.ALL, 5 )


		b_sizer11.Add( b_sizer112, 0, wx.EXPAND, 5 )

		self.m_result = wx.TextCtrl( self, wx.ID_ANY, u"Result", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		self.m_result.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_result.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )
		self.m_result.SetMinSize( wx.Size( -1,120 ) )

		b_sizer11.Add( self.m_result, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )


		b_sizer1.Add( b_sizer11, 2, wx.EXPAND, 5 )

		b_sizer12 = wx.BoxSizer( wx.VERTICAL )

		self.m_take_step = wx.Button( self, wx.ID_ANY, u"Take Step", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer12.Add( self.m_take_step, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		fg_sizer122 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fg_sizer122.AddGrowableCol( 0 )
		fg_sizer122.SetFlexibleDirection( wx.BOTH )
		fg_sizer122.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_statictext12201 = wx.StaticText( self, wx.ID_ANY, u"Check for Solved Squares", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext12201.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12201, 0, wx.ALL, 5 )

		self.m_count_solved = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_count_solved.Wrap( -1 )

		self.m_count_solved.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )
		self.m_count_solved.SetMinSize( wx.Size( 30,-1 ) )

		fg_sizer122.Add( self.m_count_solved, 0, wx.ALL, 5 )

		self.m_statictext12202 = wx.StaticText( self, wx.ID_ANY, u"Show Possibles", wx.DefaultPosition, wx.DefaultSize, 0, u"possibles" )
		self.m_statictext12202.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12202, 0, wx.ALL, 5 )

		self.m_result_step_01 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_01.Wrap( -1 )

		self.m_result_step_01.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_01, 0, wx.ALL, 5 )

		self.m_statictext12203 = wx.StaticText( self, wx.ID_ANY, u"Compartment check", wx.DefaultPosition, wx.DefaultSize, 0, u"compartment" )
		self.m_statictext12203.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12203, 0, wx.ALL, 5 )

		self.m_result_step_02 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_02.Wrap( -1 )

		self.m_result_step_02.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_02, 0, wx.ALL, 5 )

		self.m_statictext12204 = wx.StaticText( self, wx.ID_ANY, u"Mandatory Digit", wx.DefaultPosition, wx.DefaultSize, 0, u"mandatory_digit" )
		self.m_statictext12204.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12204, 0, wx.ALL, 5 )

		self.m_result_step_03 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_03.Wrap( -1 )

		self.m_result_step_03.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_03, 0, wx.ALL, 5 )

		self.m_statictext12205 = wx.StaticText( self, wx.ID_ANY, u"Stranded Digit", wx.DefaultPosition, wx.DefaultSize, 0, u"stranded_digit" )
		self.m_statictext12205.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12205, 0, wx.ALL, 5 )

		self.m_result_step_04 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_04.Wrap( -1 )

		self.m_result_step_04.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_04, 0, wx.ALL, 5 )

		self.m_statictext12206 = wx.StaticText( self, wx.ID_ANY, u"Stranded Sequence", wx.DefaultPosition, wx.DefaultSize, 0, u"stranded_seq" )
		self.m_statictext12206.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12206, 0, wx.ALL, 5 )

		self.m_result_step_05 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_05.Wrap( -1 )

		self.m_result_step_05.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_05, 0, wx.ALL, 5 )

		self.m_statictext12207 = wx.StaticText( self, wx.ID_ANY, u"Mandatory Sequence", wx.DefaultPosition, wx.DefaultSize, 0, u"mandatory_seq" )
		self.m_statictext12207.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12207, 0, wx.ALL, 5 )

		self.m_result_step_06 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_06.Wrap( -1 )

		self.m_result_step_06.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_06, 0, wx.ALL, 5 )

		self.m_statictext12208 = wx.StaticText( self, wx.ID_ANY, u"High/Low Gap", wx.DefaultPosition, wx.DefaultSize, 0, u"high_low_gap" )
		self.m_statictext12208.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12208, 0, wx.ALL, 5 )

		self.m_result_step_07 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_07.Wrap( -1 )

		self.m_result_step_07.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_07, 0, wx.ALL, 5 )

		self.m_statictext12209 = wx.StaticText( self, wx.ID_ANY, u"Isolated Digits", wx.DefaultPosition, wx.DefaultSize, 0, u"isolated_digit" )
		self.m_statictext12209.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12209, 0, wx.ALL, 5 )

		self.m_result_step_08 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_08.Wrap( -1 )

		self.m_result_step_08.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_08, 0, wx.ALL, 5 )

		self.m_statictext12210 = wx.StaticText( self, wx.ID_ANY, u"Street Range", wx.DefaultPosition, wx.DefaultSize, 0, u"street_range" )
		self.m_statictext12210.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12210, 0, wx.ALL, 5 )

		self.m_result_step_09 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_09.Wrap( -1 )

		self.m_result_step_09.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_09, 0, wx.ALL, 5 )

		self.m_statictext12211 = wx.StaticText( self, wx.ID_ANY, u"Required Digits", wx.DefaultPosition, wx.DefaultSize, 0, u"required_digit" )
		self.m_statictext12211.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12211, 0, wx.ALL, 5 )

		self.m_result_step_10 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_10.Wrap( -1 )

		self.m_result_step_10.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_10, 0, wx.ALL, 5 )

		self.m_statictext12212 = wx.StaticText( self, wx.ID_ANY, u"Naked Pairs", wx.DefaultPosition, wx.DefaultSize, 0, u"naked_pair" )
		self.m_statictext12212.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12212, 0, wx.ALL, 5 )

		self.m_result_step_11 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_11.Wrap( -1 )

		self.m_result_step_11.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_11, 0, wx.ALL, 5 )

		self.m_statictext12213 = wx.StaticText( self, wx.ID_ANY, u"Naked Triples", wx.DefaultPosition, wx.DefaultSize, 0, u"naked_triple" )
		self.m_statictext12213.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12213, 0, wx.ALL, 5 )

		self.m_result_step_12 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_12.Wrap( -1 )

		self.m_result_step_12.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_12, 0, wx.ALL, 5 )

		self.m_statictext12214 = wx.StaticText( self, wx.ID_ANY, u"Compartment High Low", wx.DefaultPosition, wx.DefaultSize, 0, u"compartment_high_low" )
		self.m_statictext12214.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12214, 0, wx.ALL, 5 )

		self.m_result_step_13 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_13.Wrap( -1 )

		self.m_result_step_13.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_13, 0, wx.ALL, 5 )

		self.m_statictext12215 = wx.StaticText( self, wx.ID_ANY, u"Range", wx.DefaultPosition, wx.DefaultSize, 0, u"range" )
		self.m_statictext12215.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12215, 0, wx.ALL, 5 )

		self.m_result_step_14 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_14.Wrap( -1 )

		self.m_result_step_14.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_14, 0, wx.ALL, 5 )

		self.m_statictext12216 = wx.StaticText( self, wx.ID_ANY, u"X-Wing", wx.DefaultPosition, wx.DefaultSize, 0, u"x_wing" )
		self.m_statictext12216.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12216, 0, wx.ALL, 5 )

		self.m_result_step_15 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_15.Wrap( -1 )

		self.m_result_step_15.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_15, 0, wx.ALL, 5 )

		self.m_statictext12217 = wx.StaticText( self, wx.ID_ANY, u"Swordfish", wx.DefaultPosition, wx.DefaultSize, 0, u"swordfish" )
		self.m_statictext12217.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12217, 0, wx.ALL, 5 )

		self.m_result_step_16 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_16.Wrap( -1 )

		self.m_result_step_16.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_16, 0, wx.ALL, 5 )

		self.m_statictext12218 = wx.StaticText( self, wx.ID_ANY, u"Jellyfish", wx.DefaultPosition, wx.DefaultSize, 0, u"jellyfish" )
		self.m_statictext12218.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12218, 0, wx.ALL, 5 )

		self.m_result_step_17 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_17.Wrap( -1 )

		self.m_result_step_17.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_17, 0, wx.ALL, 5 )

		self.m_statictext12219 = wx.StaticText( self, wx.ID_ANY, u"Starfish", wx.DefaultPosition, wx.DefaultSize, 0, u"starfish" )
		self.m_statictext12219.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12219, 0, wx.ALL, 5 )

		self.m_result_step_18 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_18.Wrap( -1 )

		self.m_result_step_18.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_18, 0, wx.ALL, 5 )

		self.m_statictext12220 = wx.StaticText( self, wx.ID_ANY, u"Setti", wx.DefaultPosition, wx.DefaultSize, 0, u"setti" )
		self.m_statictext12220.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12220, 0, wx.ALL, 5 )

		self.m_result_step_19 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_19.Wrap( -1 )

		self.m_result_step_19.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_19, 0, wx.ALL, 5 )

		self.m_statictext12221 = wx.StaticText( self, wx.ID_ANY, u"X-Y-Z", wx.DefaultPosition, wx.DefaultSize, 0, u"xyz" )
		self.m_statictext12221.Wrap( -1 )

		fg_sizer122.Add( self.m_statictext12221, 0, wx.ALL, 5 )

		self.m_result_step_20 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 30,-1 ), wx.ALIGN_RIGHT )
		self.m_result_step_20.Wrap( -1 )

		self.m_result_step_20.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		fg_sizer122.Add( self.m_result_step_20, 0, wx.ALL, 5 )


		b_sizer12.Add( fg_sizer122, 0, wx.EXPAND, 5 )

		self.m_static_text_autorun = wx.StaticText( self, wx.ID_ANY, u"Autorun active", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_static_text_autorun.Wrap( -1 )

		self.m_static_text_autorun.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )
		self.m_static_text_autorun.SetForegroundColour( wx.Colour( 255, 0, 0 ) )

		b_sizer12.Add( self.m_static_text_autorun, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_gauge_process = wx.Gauge( self, wx.ID_ANY, 10, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.m_gauge_process.SetValue( 0 )
		self.m_gauge_process.Hide()

		b_sizer12.Add( self.m_gauge_process, 0, wx.ALL|wx.EXPAND, 5 )


		b_sizer1.Add( b_sizer12, 1, wx.EXPAND, 5 )


		b_sizer0.Add( b_sizer1, 1, wx.EXPAND, 5 )


		self.SetSizer( b_sizer0 )
		self.Layout()
		b_sizer0.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_create_new.Bind( wx.EVT_BUTTON, self.create_new_game )
		self.m_savegame.Bind( wx.EVT_BUTTON, self.save_game )
		self.m_batchmode.Bind( wx.EVT_TOGGLEBUTTON, self.batch_mode )
		self.m_select_game.Bind( wx.EVT_CHOICE, self.select_game )
		self.m_guess.Bind( wx.EVT_BUTTON, self.guess_next )
		self.m_settings.Bind( wx.EVT_BUTTON, self.set_config )
		self.m_take_step.Bind( wx.EVT_BUTTON, self.next_step )
		self.m_statictext12201.Bind( wx.EVT_LEFT_UP, self.calc_solverstring )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def create_new_game( self, event ):
		event.Skip()

	def save_game( self, event ):
		event.Skip()

	def batch_mode( self, event ):
		event.Skip()

	def select_game( self, event ):
		event.Skip()

	def guess_next( self, event ):
		event.Skip()

	def set_config( self, event ):
		event.Skip()

	def next_step( self, event ):
		event.Skip()

	def calc_solverstring( self, event ):
		event.Skip()


###########################################################################
## Class DigitPanel
###########################################################################

class DigitPanel ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		b_sizer1 = wx.BoxSizer( wx.VERTICAL )

		self.Digit = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
		self.Digit.SetBackgroundColour( wx.Colour( 128, 128, 128 ) )

		b_sizer11 = wx.BoxSizer( wx.VERTICAL )

		self.m_digit_text = wx.StaticText( self.Digit, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,60 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_digit_text.Wrap( -1 )

		self.m_digit_text.SetFont( wx.Font( 40, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_digit_text.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_digit_text.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		b_sizer11.Add( self.m_digit_text, 1, wx.EXPAND, 1 )


		self.Digit.SetSizer( b_sizer11 )
		self.Digit.Layout()
		b_sizer11.Fit( self.Digit )
		b_sizer1.Add( self.Digit, 0, 0, 5 )

		self.Digits = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.Digits.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.Digits.SetBackgroundColour( wx.Colour( 128, 128, 128 ) )

		g_sizer12 = wx.GridSizer( 3, 3, 0, 0 )

		self.m_statictext121 = wx.StaticText( self.Digits, wx.ID_ANY, u"1", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext121.Wrap( -1 )

		self.m_statictext121.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext121.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext121, 0, 0, 1 )

		self.m_statictext122 = wx.StaticText( self.Digits, wx.ID_ANY, u"2", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext122.Wrap( -1 )

		self.m_statictext122.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext122.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext122, 0, 0, 1 )

		self.m_statictext123 = wx.StaticText( self.Digits, wx.ID_ANY, u"3", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext123.Wrap( -1 )

		self.m_statictext123.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext123.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext123, 0, 0, 1 )

		self.m_statictext124 = wx.StaticText( self.Digits, wx.ID_ANY, u"4", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext124.Wrap( -1 )

		self.m_statictext124.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext124.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext124, 0, 0, 1 )

		self.m_statictext125 = wx.StaticText( self.Digits, wx.ID_ANY, u"5", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext125.Wrap( -1 )

		self.m_statictext125.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext125.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext125, 0, 0, 1 )

		self.m_statictext126 = wx.StaticText( self.Digits, wx.ID_ANY, u"6", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext126.Wrap( -1 )

		self.m_statictext126.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext126.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext126, 0, 0, 1 )

		self.m_statictext127 = wx.StaticText( self.Digits, wx.ID_ANY, u"7", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext127.Wrap( -1 )

		self.m_statictext127.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext127.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext127, 0, 0, 1 )

		self.m_statictext128 = wx.StaticText( self.Digits, wx.ID_ANY, u"8", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext128.Wrap( -1 )

		self.m_statictext128.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext128.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext128, 0, 0, 1 )

		self.m_statictext129 = wx.StaticText( self.Digits, wx.ID_ANY, u"9", wx.DefaultPosition, wx.Size( 20,20 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext129.Wrap( -1 )

		self.m_statictext129.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_statictext129.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		g_sizer12.Add( self.m_statictext129, 0, 0, 1 )


		self.Digits.SetSizer( g_sizer12 )
		self.Digits.Layout()
		g_sizer12.Fit( self.Digits )
		b_sizer1.Add( self.Digits, 0, 0, 5 )


		self.SetSizer( b_sizer1 )
		self.Layout()
		b_sizer1.Fit( self )

		# Connect Events
		self.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_digit_text.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext121.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext122.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext123.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext124.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext125.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext126.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext127.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext128.Bind( wx.EVT_LEFT_UP, self.on_key_edit )
		self.m_statictext129.Bind( wx.EVT_LEFT_UP, self.on_key_edit )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_key_edit( self, event ):
		event.Skip()












###########################################################################
## Class ConfigDialog
###########################################################################

class ConfigDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,350 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.Size( -1,-1 ), wx.DefaultSize )

		b_sizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_statictext11 = wx.StaticText( self, wx.ID_ANY, u"Settings:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext11.Wrap( -1 )

		b_sizer1.Add( self.m_statictext11, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_file_picker = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.txt", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE|wx.FLP_FILE_MUST_EXIST|wx.FLP_OPEN|wx.FLP_USE_TEXTCTRL )
		b_sizer1.Add( self.m_file_picker, 0, wx.ALL|wx.EXPAND, 5 )

		gb_sizer11 = wx.GridBagSizer( 0, 0 )
		gb_sizer11.SetFlexibleDirection( wx.BOTH )
		gb_sizer11.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_singlestep = wx.CheckBox( self, wx.ID_ANY, u"Single Step", wx.DefaultPosition, wx.DefaultSize, 0 )
		gb_sizer11.Add( self.m_singlestep, wx.GBPosition( 0, 0 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.EXPAND, 5 )

		self.m_autorun_single = wx.CheckBox( self, wx.ID_ANY, u"Autorun Solver", wx.DefaultPosition, wx.DefaultSize, 0 )
		gb_sizer11.Add( self.m_autorun_single, wx.GBPosition( 1, 0 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.EXPAND, 5 )

		b_sizer111 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel_autorun_data = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		b_sizer1111 = wx.BoxSizer( wx.VERTICAL )

		self.m_slider_delay = wx.Slider( self.m_panel_autorun_data, wx.ID_ANY, 400, 100, 1000, wx.DefaultPosition, wx.DefaultSize, wx.SL_AUTOTICKS|wx.SL_HORIZONTAL|wx.SL_LABELS )
		b_sizer1111.Add( self.m_slider_delay, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_autorun_file = wx.CheckBox( self.m_panel_autorun_data, wx.ID_ANY, u"Autorun on File", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer1111.Add( self.m_autorun_file, 0, wx.ALL|wx.EXPAND, 5 )


		self.m_panel_autorun_data.SetSizer( b_sizer1111 )
		self.m_panel_autorun_data.Layout()
		b_sizer1111.Fit( self.m_panel_autorun_data )
		b_sizer111.Add( self.m_panel_autorun_data, 1, wx.ALL|wx.EXPAND, 5 )


		gb_sizer11.Add( b_sizer111, wx.GBPosition( 2, 1 ), wx.GBSpan( 1, 1 ), wx.EXPAND, 5 )

		self.m_stop_on_success = wx.CheckBox( self, wx.ID_ANY, u"Stop on Success", wx.DefaultPosition, wx.DefaultSize, 0 )
		gb_sizer11.Add( self.m_stop_on_success, wx.GBPosition( 3, 0 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.EXPAND, 5 )

		self.m_stop_on_failure = wx.CheckBox( self, wx.ID_ANY, u"Stop on Failure", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_stop_on_failure.SetValue(True)
		gb_sizer11.Add( self.m_stop_on_failure, wx.GBPosition( 4, 0 ), wx.GBSpan( 1, 2 ), wx.ALL|wx.EXPAND, 5 )


		gb_sizer11.AddGrowableCol( 1 )
		gb_sizer11.AddGrowableRow( 1 )
		gb_sizer11.AddGrowableRow( 2 )
		gb_sizer11.AddGrowableRow( 3 )

		b_sizer1.Add( gb_sizer11, 0, wx.EXPAND, 5 )

		b_sizer12 = wx.BoxSizer( wx.HORIZONTAL )


		b_sizer12.Add( ( 0, 0), 1, 0, 5 )

		m_sdb_sizer12 = wx.StdDialogButtonSizer()
		self.m_sdb_sizer12Save = wx.Button( self, wx.ID_SAVE )
		m_sdb_sizer12.AddButton( self.m_sdb_sizer12Save )
		self.m_sdb_sizer12Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdb_sizer12.AddButton( self.m_sdb_sizer12Cancel )
		m_sdb_sizer12.Realize()

		b_sizer12.Add( m_sdb_sizer12, 0, 0, 5 )


		b_sizer1.Add( b_sizer12, 0, wx.EXPAND, 5 )


		self.SetSizer( b_sizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_autorun_single.Bind( wx.EVT_CHECKBOX, self.set_autorun_panel )
		self.m_sdb_sizer12Save.Bind( wx.EVT_BUTTON, self.save_config )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def set_autorun_panel( self, event ):
		event.Skip()

	def save_config( self, event ):
		event.Skip()


###########################################################################
## Class TextInput
###########################################################################

class TextInput ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.DefaultSize, style = 0 )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		b_sizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_text_input = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		b_sizer1.Add( self.m_text_input, 0, wx.ALL, 5 )


		self.SetSizer( b_sizer1 )
		self.Layout()
		b_sizer1.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_text_input.Bind( wx.EVT_TEXT_ENTER, self.edit_ready )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def edit_ready( self, event ):
		event.Skip()


###########################################################################
## Class CreateDialog
###########################################################################

class CreateDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		fg_sizer1 = wx.FlexGridSizer( 2, 2, 0, 0 )
		fg_sizer1.SetFlexibleDirection( wx.BOTH )
		fg_sizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_bpButton5 = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_bpButton5.SetBitmap( wx.ArtProvider.GetBitmap( u"gtk-remove",  ) )
		fg_sizer1.Add( self.m_bpButton5, 0, 0, 5 )

		b_sizer11 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext11111 = wx.StaticText( self, wx.ID_ANY, u"1", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11111.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11111, 1, wx.ALL, 5 )

		self.m_statictext11112 = wx.StaticText( self, wx.ID_ANY, u"2", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11112.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11112, 1, wx.ALL, 5 )

		self.m_statictext11113 = wx.StaticText( self, wx.ID_ANY, u"3", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11113.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11113, 1, wx.ALL, 5 )

		self.m_statictext11114 = wx.StaticText( self, wx.ID_ANY, u"4", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11114.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11114, 1, wx.ALL, 5 )

		self.m_statictext11115 = wx.StaticText( self, wx.ID_ANY, u"5", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11115.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11115, 1, wx.ALL, 5 )

		self.m_statictext11116 = wx.StaticText( self, wx.ID_ANY, u"6", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11116.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11116, 1, wx.ALL, 5 )

		self.m_statictext11117 = wx.StaticText( self, wx.ID_ANY, u"7", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11117.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11117, 1, wx.ALL, 5 )

		self.m_statictext11118 = wx.StaticText( self, wx.ID_ANY, u"8", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11118.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11118, 1, wx.ALL, 5 )

		self.m_statictext11119 = wx.StaticText( self, wx.ID_ANY, u"9", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_statictext11119.Wrap( -1 )

		b_sizer11.Add( self.m_statictext11119, 1, wx.ALL, 5 )


		fg_sizer1.Add( b_sizer11, 1, wx.EXPAND, 5 )

		b_sizer12 = wx.BoxSizer( wx.VERTICAL )

		b_sizer11121 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111211 = wx.StaticText( self, wx.ID_ANY, u"A", wx.Point( -1,-1 ), wx.DefaultSize, 0 )
		self.m_statictext111211.Wrap( -1 )

		b_sizer11121.Add( self.m_statictext111211, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11121, 1, wx.EXPAND, 5 )

		b_sizer11122 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111221 = wx.StaticText( self, wx.ID_ANY, u"B", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111221.Wrap( -1 )

		b_sizer11122.Add( self.m_statictext111221, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11122, 1, wx.EXPAND, 5 )

		b_sizer11123 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111231 = wx.StaticText( self, wx.ID_ANY, u"C", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111231.Wrap( -1 )

		b_sizer11123.Add( self.m_statictext111231, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11123, 1, wx.EXPAND, 5 )

		b_sizer11124 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111241 = wx.StaticText( self, wx.ID_ANY, u"D", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111241.Wrap( -1 )

		b_sizer11124.Add( self.m_statictext111241, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11124, 1, wx.EXPAND, 5 )

		b_sizer11125 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111251 = wx.StaticText( self, wx.ID_ANY, u"E", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111251.Wrap( -1 )

		b_sizer11125.Add( self.m_statictext111251, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11125, 1, wx.EXPAND, 5 )

		b_sizer11126 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111261 = wx.StaticText( self, wx.ID_ANY, u"F", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111261.Wrap( -1 )

		b_sizer11126.Add( self.m_statictext111261, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11126, 1, wx.EXPAND, 5 )

		b_sizer11127 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111271 = wx.StaticText( self, wx.ID_ANY, u"G", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111271.Wrap( -1 )

		b_sizer11127.Add( self.m_statictext111271, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11127, 1, wx.EXPAND, 5 )

		b_sizer11128 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111281 = wx.StaticText( self, wx.ID_ANY, u"H", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111281.Wrap( -1 )

		b_sizer11128.Add( self.m_statictext111281, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11128, 1, wx.EXPAND, 5 )

		b_sizer11129 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_statictext111291 = wx.StaticText( self, wx.ID_ANY, u"J", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_statictext111291.Wrap( -1 )

		b_sizer11129.Add( self.m_statictext111291, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		b_sizer12.Add( b_sizer11129, 1, wx.EXPAND, 5 )


		fg_sizer1.Add( b_sizer12, 1, wx.EXPAND, 5 )

		self.m_gameboard = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
		self.m_gameboard.SetBackgroundColour( wx.Colour( 0, 0, 0 ) )

		g_sizer13 = wx.GridSizer( 9, 9, 0, 0 )

		g_sizer13.SetMinSize( wx.Size( 270,270 ) )

		self.m_gameboard.SetSizer( g_sizer13 )
		self.m_gameboard.Layout()
		g_sizer13.Fit( self.m_gameboard )
		fg_sizer1.Add( self.m_gameboard, 0, 0, 5 )


		self.SetSizer( fg_sizer1 )
		self.Layout()
		fg_sizer1.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_bpButton5.Bind( wx.EVT_BUTTON, self.clean_field )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def clean_field( self, event ):
		event.Skip()


###########################################################################
## Class DigitInput
###########################################################################

class DigitInput ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		b_sizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_digit_text = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 28,28 ), wx.ALIGN_CENTER_HORIZONTAL )
		self.m_digit_text.Wrap( -1 )

		self.m_digit_text.SetFont( wx.Font( 16, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_digit_text.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.m_digit_text.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		b_sizer1.Add( self.m_digit_text, 1, wx.EXPAND, 1 )


		self.SetSizer( b_sizer1 )
		self.Layout()
		b_sizer1.Fit( self )

		# Connect Events
		self.m_digit_text.Bind( wx.EVT_LEFT_UP, self.on_left_up )
		self.m_digit_text.Bind( wx.EVT_RIGHT_DOWN, self.on_right_down )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_left_up( self, event ):
		event.Skip()

	def on_right_down( self, event ):
		event.Skip()


