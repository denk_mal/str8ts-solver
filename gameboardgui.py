#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=R1702,R0912,R0913,R0914,R0915

import wx

import str8tssolver_gui


class GameboardGUI:
    _ROW_MARKER = "ABCDEFGHJ"
    _COLUMN_MARKER = "123456789"
    _DIGITS = "123456789"

    def __init__(self, parent: str8tssolver_gui):
        self._gui: str8tssolver_gui = parent
        self._playground = [['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','',''],
                            ['','','','','','','','','','']]

    def init_field(self, content: str, mask: str) -> bool:
        """ initiate the gameboard with a predefined game

        :param content: numbers in the field (81 chars); 0 = empty field
        :param mask: blocks in the field (81 chars); 1 = block
        :return: True on success
        """
        if len(content) != 81:
            print("wrong length of content (must be 81 chars)")
            return False
        if len(mask) != 81:
            print("wrong length of mask (must be 81 chars)")
            return False
        if self._gui:
            sizer = self._gui.m_gameboard.GetSizer()
            sizer.Clear(True)
        else:
            sizer = None
        for idx, digit in enumerate(content):
            mode = 0
            if mask[idx] == "0":  # black field
                mode = 10
            mode += int(digit)
            y, x = divmod(idx, 9)
            if self._gui:
                new_field = str8tssolver_gui.DigitPanelGui(self._gui.m_gameboard, mode, x, y)
                sizer.Add(new_field, 1, wx.ALL | wx.EXPAND, 1)
            if mode > 10:
                self._playground[x][y] = str(mode - 10)
            elif mode == 10:
                self._playground[x][y] = GameboardGUI._DIGITS
            elif mode > 0:
                self._playground[x][y] = f"-{mode}"
            else:
                self._playground[x][y] = "0"
        if self._gui:
            self._gui.SendSizeEventToParent()
            self._gui.Layout()
        return True

    def has_gui(self) -> bool:
        return self._gui is not None

    def add_result_text(self, prefix, old_content, new_content, x, y, obj=""):
        if self._gui:
            self._gui.add_result_text(prefix, old_content, new_content, x, y, obj)

    @staticmethod
    def convert_coords_to_gui(x: int, y: int) -> str:
        """ converts coordinates to human readable format

        :param x: column
        :param y: row
        :return: string of coordinate (A-J 1-9)
        """
        return GameboardGUI._ROW_MARKER[y] + GameboardGUI._COLUMN_MARKER[x]

    def display_playground(self) -> None:
        """ transfer gameboard to gui

        :return: None
        """
        if not self._gui:
            return
        for y in range(9):
            for x in range(9):
                self._gui.set_label_text(x + 1, y + 1, self._playground[x][y], False, 0)

    def get_content(self, x: int, y: int, subset: str = None) -> str:
        """ get the content of a field

        :param x: column
        :param y: row
        :param subset: filter for content; default: None
        :return: the content of a field reduced by a filter if set
        """
        content = self._playground[x][y]
        if subset:
            for char in GameboardGUI._DIGITS:
                if char not in subset:
                    content = content.replace(char, "")
        return content

    def set_content(self, x: int, y: int, content: str) -> None:
        """ set content of a field

        :param x: column
        :param y: row
        :param content: content for the field
        :return: None
        """
        self._playground[x][y] = content

    def get_gameboard_string(self) -> str:
        """ returns the gameboard as a string

        :return: combined content and mask (162 chars)
        """
        content = ""
        mask = ""
        for y in range(9):
            for x in range(9):
                if self._playground[x][y]:
                    field = int(self._playground[x][y])
                    if field < 10:
                        content += str(abs(field))
                    else:
                        content += "0"
                    if field > 0:
                        mask += "0"
                    else:
                        mask += "1"
                else:
                    content += "0"
                    mask += "0"
        return f"{content}{mask}"

    def get_singledigit(self, x: int, y: int) -> str:
        """ returns the digit if field contains a single digit

        :param x: column
        :param y: row
        :return: the single digit if field contains a single digit, otherwise "0"
        """
        cmp_value = int(self._playground[x][y])
        if cmp_value > 9:
            return "0"
        return str(abs(cmp_value))

    def is_unsolved(self, x: int, y: int) -> bool:
        """ check if field is unsolved

        :param x: column
        :param y: row
        :return: true if field contains more than one candidate
        """
        return int(self._playground[x][y]) > 9

if __name__ == '__main__':
    import str8tssolver
    str8tssolver.main()
