#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=R1702,R0912,R0913,R0914,R0915,R0916

from typing import Tuple, List

import gameboardcore
import str8tssolver_gui


class Gameboard(gameboardcore.GameboardCore):
    def __init__(self, parent: str8tssolver_gui):
        super().__init__(parent)
        self._row_column_counts = []
        self._row_counts = []
        self._column_counts = []
        self._rows_to_check = {}
        self._columns_to_check = {}

    def init_combined_environment(self) -> None:
        """ init environment for subroutines, that acts on multiple fields like e.g x-wing, setti

        :return: None
        """
        self._row_column_counts = []
        self._row_counts = []
        self._column_counts = []

    def _init_counts(self) -> None:
        if self._row_column_counts:
            return
        self._row_column_counts = [[[0] * 9 for _ in range(9)] for _ in range(9)]
        self._row_counts = [[0] * 9 for _ in range(9)]
        self._column_counts = [[0] * 9 for _ in range(9)]
        self._rows_to_check = {}
        self._columns_to_check = {}
        for idx in range(9):
            digit = str(idx + 1)
            for y in range(9):
                row = self.necessary_row(-1, y)
                for x in range(9):
                    column = self.necessary_column(x, -1)
                    if digit in self.get_content(x, y):
                        if digit in row and digit in column:
                            self._row_column_counts[idx][y][x] = 1
                            self._row_counts[idx][y] += 1
                            self._column_counts[idx][x] += 1
                        else:
                            self._row_column_counts[idx][y][x] = -1

    def _init_fish(self, base: int) -> None:
        """ generate internal row and column check tables

        :param base: base for the check (2 -> x-wing, 3 -> swordfish, 4 -> jellyfish, 5 -> starfish)
        :return: None
        """
        self._init_counts()
        if base in self._rows_to_check:
            return

        self._rows_to_check[base] = []
        self._columns_to_check[base] = []
        for idx in range(9):
            self._rows_to_check[base].append([])
            self._columns_to_check[base].append([])
            for xy in range(9):
                if 2 <= self._row_counts[idx][xy] <= base:
                    self._rows_to_check[base][idx].append(xy)
                if 2 <= self._column_counts[idx][xy] <= base:
                    self._columns_to_check[base][idx].append(xy)

    def x_wing_row_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(bottom,right) of an existing row
            based x-wing

        :param digit:
        :return: list of 2-tuple
        """
        result = []
        self._init_fish(2)
        digit_int = int(digit) - 1
        if len(self._rows_to_check[2][digit_int]) > 1:
            idx1 = 0
            for row1 in self._rows_to_check[2][digit_int]:
                idx1 += 1
                for row2 in self._rows_to_check[2][digit_int][idx1:]:
                    pair_cnt = 0
                    x1 = -1
                    x2 = -1
                    for column in range(9):
                        if self._row_column_counts[digit_int][row1][column] == 1 and \
                                self._row_column_counts[digit_int][row2][column] == 1:
                            pair_cnt += 1
                            if x1 == -1:
                                x1 = column
                            elif x2 == -1:
                                x2 = column
                        if self._row_column_counts[digit_int][row1][column] == -1 or \
                                self._row_column_counts[digit_int][row2][column] == -1:
                            pair_cnt = 99
                    if pair_cnt == 2:
                        result.append((x1, row1))
                        result.append((x2, row2))
                    if 2 < pair_cnt < 99:
                        return []  # FEHLER
        return result

    def x_wing_column_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(bottom,right) of an existing column
            based x-wing

        :param digit:
        :return: list of 2-tuple
        """
        result = []
        self._init_fish(2)
        digit_int = int(digit) - 1
        if len(self._columns_to_check[2][digit_int]) > 1:
            idx1 = 0
            for col1 in self._columns_to_check[2][digit_int]:
                idx1 += 1
                for col2 in self._columns_to_check[2][digit_int][idx1:]:
                    pair_cnt = 0
                    y1 = -1
                    y2 = -1
                    for row in range(9):
                        if self._row_column_counts[digit_int][row][col1] == 1 and \
                                self._row_column_counts[digit_int][row][col2] == 1:
                            pair_cnt += 1
                            if y1 == -1:
                                y1 = row
                            elif y2 == -1:
                                y2 = row
                        if self._row_column_counts[digit_int][row][col1] == -1 or \
                                self._row_column_counts[digit_int][row][col2] == -1:
                            pair_cnt = 99
                    if pair_cnt == 2:
                        result.append((col1, y1))
                        result.append((col2, y2))
                    if 2 < pair_cnt < 99:
                        return []  # FEHLER
        return result

    def swordfish_row_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(middle, middle)(bottom,right) of an existing
            row based swordfish

        :param digit:
        :return: list of 3-tuple
        """
        result = []
        self._init_fish(3)
        digit_int = int(digit) - 1
        if len(self._rows_to_check[3][digit_int]) > 2:
            idx1 = 0
            for row1 in self._rows_to_check[3][digit_int]:
                idx1 += 1
                idx2 = idx1
                for row2 in self._rows_to_check[3][digit_int][idx1:]:
                    idx2 += 1
                    for row3 in self._rows_to_check[3][digit_int][idx2:]:
                        pair_cnt = 0
                        x1 = -1
                        x2 = -1
                        x3 = -1
                        for column in range(9):
                            if (self._row_column_counts[digit_int][row1][column] == 1 and
                                self._row_column_counts[digit_int][row2][column] == 1) or \
                                    (self._row_column_counts[digit_int][row1][column] == 1 and
                                     self._row_column_counts[digit_int][row3][column] == 1) or \
                                    (self._row_column_counts[digit_int][row2][column] == 1 and
                                     self._row_column_counts[digit_int][row3][column] == 1):
                                pair_cnt += 1
                                if x1 == -1:
                                    x1 = column
                                elif x2 == -1:
                                    x2 = column
                                elif x3 == -1:
                                    x3 = column
                            if self._row_column_counts[digit_int][row1][column] == -1 or \
                                    self._row_column_counts[digit_int][row2][column] == -1 or \
                                    self._row_column_counts[digit_int][row3][column] == -1:
                                pair_cnt = 99
                        if pair_cnt == 3:
                            found_outside = False
                            for column in range(9):
                                if column in (x1, x2, x3):
                                    continue
                                if self._row_column_counts[digit_int][row1][column] == 1 or \
                                        self._row_column_counts[digit_int][row2][column] == 1 or \
                                        self._row_column_counts[digit_int][row3][column] == 1:
                                    found_outside = True
                            if not found_outside:
                                result.append((x1, row1))
                                result.append((x2, row2))
                                result.append((x3, row3))
                        if 3 < pair_cnt < 99:
                            return []  # FEHLER
        return result

    def swordfish_column_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(middle, middle)(bottom,right) of an existing
            row based swordfish

        :param digit:
        :return: list of 3-tuple
        """
        result = []
        self._init_fish(3)
        digit_int = int(digit) - 1
        if len(self._columns_to_check[3][digit_int]) > 2:
            idx1 = 0
            for col1 in self._columns_to_check[3][digit_int]:
                idx1 += 1
                idx2 = idx1
                for col2 in self._columns_to_check[3][digit_int][idx1:]:
                    idx2 += 1
                    for col3 in self._columns_to_check[3][digit_int][idx2:]:
                        pair_cnt = 0
                        y1 = -1
                        y2 = -1
                        y3 = -1
                        for row in range(9):
                            if (self._row_column_counts[digit_int][row][col1] == 1 and
                                self._row_column_counts[digit_int][row][col2] == 1) or \
                                    (self._row_column_counts[digit_int][row][col1] == 1 and
                                     self._row_column_counts[digit_int][row][col3] == 1) or \
                                    (self._row_column_counts[digit_int][row][col2] == 1 and
                                     self._row_column_counts[digit_int][row][col3] == 1):
                                pair_cnt += 1
                                if y1 == -1:
                                    y1 = row
                                elif y2 == -1:
                                    y2 = row
                                elif y3 == -1:
                                    y3 = row
                            if self._row_column_counts[digit_int][row][col1] == -1 or \
                                    self._row_column_counts[digit_int][row][col2] == -1 or \
                                    self._row_column_counts[digit_int][row][col3] == -1:
                                pair_cnt = 99
                        if pair_cnt == 3:
                            found_outside = False
                            for row in range(9):
                                if row in (y1, y2, y3):
                                    continue
                                if self._row_column_counts[digit_int][row][col1] == 1 or \
                                        self._row_column_counts[digit_int][row][col2] == 1 or \
                                        self._row_column_counts[digit_int][row][col3] == 1:
                                    found_outside = True
                            if not found_outside:
                                result.append((col1, y1))
                                result.append((col2, y2))
                                result.append((col3, y3))
                        if 3 < pair_cnt < 99:
                            return []  # FEHLER
        return result

    def jellyfish_row_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(middle1, middle1)(middle2, middle2)(bottom,right) of an existing
            row based jellyfish

        :param digit:
        :return: list of 4-tuple
        """
        result = []
        self._init_fish(4)
        digit_int = int(digit) - 1
        if len(self._rows_to_check[4][digit_int]) > 3:
            idx1 = 0
            for row1 in self._rows_to_check[4][digit_int]:
                idx1 += 1
                idx2 = idx1
                for row2 in self._rows_to_check[4][digit_int][idx1:]:
                    idx2 += 1
                    idx3 = idx2
                    for row3 in self._rows_to_check[4][digit_int][idx2:]:
                        idx3 += 1
                        for row4 in self._rows_to_check[4][digit_int][idx3:]:
                            pair_cnt = 0
                            x1 = -1
                            x2 = -1
                            x3 = -1
                            x4 = -1
                            for column in range(9):
                                if (self._row_column_counts[digit_int][row1][column] == 1 and
                                    self._row_column_counts[digit_int][row2][column] == 1) or \
                                        (self._row_column_counts[digit_int][row1][column] == 1 and
                                         self._row_column_counts[digit_int][row3][column] == 1) or \
                                        (self._row_column_counts[digit_int][row1][column] == 1 and
                                         self._row_column_counts[digit_int][row4][column] == 1) or \
                                        (self._row_column_counts[digit_int][row2][column] == 1 and
                                         self._row_column_counts[digit_int][row3][column] == 1) or \
                                        (self._row_column_counts[digit_int][row2][column] == 1 and
                                         self._row_column_counts[digit_int][row4][column] == 1) or \
                                        (self._row_column_counts[digit_int][row3][column] == 1 and
                                         self._row_column_counts[digit_int][row4][column] == 1):
                                    pair_cnt += 1
                                    if x1 == -1:
                                        x1 = column
                                    elif x2 == -1:
                                        x2 = column
                                    elif x3 == -1:
                                        x3 = column
                                    elif x4 == -1:
                                        x4 = column
                                if self._row_column_counts[digit_int][row1][column] == -1 or \
                                        self._row_column_counts[digit_int][row2][column] == -1 or \
                                        self._row_column_counts[digit_int][row3][column] == -1 or \
                                        self._row_column_counts[digit_int][row4][column] == -1:
                                    pair_cnt = 99
                            if pair_cnt == 4:
                                found_outside = False
                                for column in range(9):
                                    if column in (x1, x2, x3, x4):
                                        continue
                                    if self._row_column_counts[digit_int][row1][column] == 1 or \
                                            self._row_column_counts[digit_int][row2][column] == 1 or \
                                            self._row_column_counts[digit_int][row3][column] == 1 or \
                                            self._row_column_counts[digit_int][row4][column] == 1:
                                        found_outside = True
                                if not found_outside:
                                    result.append((x1, row1))
                                    result.append((x2, row2))
                                    result.append((x3, row3))
                                    result.append((x4, row4))
                            if 4 < pair_cnt < 99:
                                return []  # FEHLER
        return result

    def jellyfish_column_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(middle1, middle1)(middle2, middle2)(bottom,right) of an existing
            column based jellyfish

        :param digit:
        :return: list of 4-tuple
        """
        result = []
        self._init_fish(4)
        digit_int = int(digit) - 1
        if len(self._columns_to_check[4][digit_int]) > 3:
            idx1 = 0
            for col1 in self._columns_to_check[4][digit_int]:
                idx1 += 1
                idx2 = idx1
                for col2 in self._columns_to_check[4][digit_int][idx1:]:
                    idx2 += 1
                    idx3 = idx2
                    for col3 in self._columns_to_check[4][digit_int][idx2:]:
                        idx3 += 1
                        for col4 in self._columns_to_check[4][digit_int][idx3:]:
                            pair_cnt = 0
                            y1 = -1
                            y2 = -1
                            y3 = -1
                            y4 = -1
                            for row in range(9):
                                if (self._row_column_counts[digit_int][row][col1] == 1 and
                                    self._row_column_counts[digit_int][row][col2] == 1) or \
                                        (self._row_column_counts[digit_int][row][col1] == 1 and
                                         self._row_column_counts[digit_int][row][col3] == 1) or \
                                        (self._row_column_counts[digit_int][row][col1] == 1 and
                                         self._row_column_counts[digit_int][row][col4] == 1) or \
                                        (self._row_column_counts[digit_int][row][col2] == 1 and
                                         self._row_column_counts[digit_int][row][col3] == 1) or \
                                        (self._row_column_counts[digit_int][row][col2] == 1 and
                                         self._row_column_counts[digit_int][row][col4] == 1) or \
                                        (self._row_column_counts[digit_int][row][col3] == 1 and
                                         self._row_column_counts[digit_int][row][col4] == 1):
                                    pair_cnt += 1
                                    if y1 == -1:
                                        y1 = row
                                    elif y2 == -1:
                                        y2 = row
                                    elif y3 == -1:
                                        y3 = row
                                    elif y4 == -1:
                                        y4 = row
                                if self._row_column_counts[digit_int][row][col1] == -1 or \
                                        self._row_column_counts[digit_int][row][col2] == -1 or \
                                        self._row_column_counts[digit_int][row][col3] == -1 or \
                                        self._row_column_counts[digit_int][row][col4] == -1:
                                    pair_cnt = 99
                            if pair_cnt == 4:
                                found_outside = False
                                for row in range(9):
                                    if row in (y1, y2, y3, y4):
                                        continue
                                    if self._row_column_counts[digit_int][row][col1] == 1 or \
                                            self._row_column_counts[digit_int][row][col2] == 1 or \
                                            self._row_column_counts[digit_int][row][col3] == 1 or \
                                            self._row_column_counts[digit_int][row][col4] == 1:
                                        found_outside = True
                                if not found_outside:
                                    result.append((col1, y1))
                                    result.append((col2, y2))
                                    result.append((col3, y3))
                                    result.append((col4, y4))
                            if 4 < pair_cnt < 99:
                                return []  # FEHLER
        return result

    def starfish_row_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(middle1, middle1)(middle2, middle2)(middle3, middle3)(bottom,right) of
            an existing row based starfish

        :param digit:
        :return: list of 5-tuple
        """
        result = []
        self._init_fish(5)
        digit_int = int(digit) - 1
        if len(self._rows_to_check[5][digit_int]) > 4:
            idx1 = 0
            for row1 in self._rows_to_check[5][digit_int]:
                idx1 += 1
                idx2 = idx1
                for row2 in self._rows_to_check[5][digit_int][idx1:]:
                    idx2 += 1
                    idx3 = idx2
                    for row3 in self._rows_to_check[5][digit_int][idx2:]:
                        idx3 += 1
                        idx4 = idx3
                        for row4 in self._rows_to_check[5][digit_int][idx3:]:
                            idx4 += 1
                            for row5 in self._rows_to_check[5][digit_int][idx4:]:
                                pair_cnt = 0
                                x1 = -1
                                x2 = -1
                                x3 = -1
                                x4 = -1
                                x5 = -1
                                for column in range(9):
                                    if (self._row_column_counts[digit_int][row1][column] == 1 and
                                        self._row_column_counts[digit_int][row2][column] == 1) or \
                                            (self._row_column_counts[digit_int][row1][column] == 1 and
                                             self._row_column_counts[digit_int][row3][column] == 1) or \
                                            (self._row_column_counts[digit_int][row1][column] == 1 and
                                             self._row_column_counts[digit_int][row4][column] == 1) or \
                                            (self._row_column_counts[digit_int][row1][column] == 1 and
                                             self._row_column_counts[digit_int][row5][column] == 1) or \
                                            (self._row_column_counts[digit_int][row2][column] == 1 and
                                             self._row_column_counts[digit_int][row3][column] == 1) or \
                                            (self._row_column_counts[digit_int][row2][column] == 1 and
                                             self._row_column_counts[digit_int][row4][column] == 1) or \
                                            (self._row_column_counts[digit_int][row2][column] == 1 and
                                             self._row_column_counts[digit_int][row5][column] == 1) or \
                                            (self._row_column_counts[digit_int][row3][column] == 1 and
                                             self._row_column_counts[digit_int][row4][column] == 1) or \
                                            (self._row_column_counts[digit_int][row3][column] == 1 and
                                             self._row_column_counts[digit_int][row5][column] == 1) or \
                                            (self._row_column_counts[digit_int][row4][column] == 1 and
                                             self._row_column_counts[digit_int][row5][column] == 1):
                                        pair_cnt += 1
                                        if x1 == -1:
                                            x1 = column
                                        elif x2 == -1:
                                            x2 = column
                                        elif x3 == -1:
                                            x3 = column
                                        elif x4 == -1:
                                            x4 = column
                                        elif x5 == -1:
                                            x5 = column
                                    if self._row_column_counts[digit_int][row1][column] == -1 or \
                                            self._row_column_counts[digit_int][row2][column] == -1 or \
                                            self._row_column_counts[digit_int][row3][column] == -1 or \
                                            self._row_column_counts[digit_int][row4][column] == -1 or \
                                            self._row_column_counts[digit_int][row5][column] == -1:
                                        pair_cnt = 99
                                if pair_cnt == 5:
                                    found_outside = False
                                    for column in range(9):
                                        if column in (x1, x2, x3, x4, x5):
                                            continue
                                        if self._row_column_counts[digit_int][row1][column] == 1 or \
                                                self._row_column_counts[digit_int][row2][column] == 1 or \
                                                self._row_column_counts[digit_int][row3][column] == 1 or \
                                                self._row_column_counts[digit_int][row4][column] == 1 or \
                                                self._row_column_counts[digit_int][row5][column] == 1:
                                            found_outside = True
                                    if not found_outside:
                                        result.append((x1, row1))
                                        result.append((x2, row2))
                                        result.append((x3, row3))
                                        result.append((x4, row4))
                                        result.append((x5, row5))
                                if 5 < pair_cnt < 99:
                                    return []  # FEHLER
        return result

    def starfish_column_coords(self, digit: str) -> List[Tuple[int, int]]:
        """ returns the coordinates (top,left)(middle1, middle1)(middle2, middle2)(middle3, middle3)(bottom,right) of
            an existing column based starfish

        :param digit:
        :return: list of 5-tuple
        """
        result = []
        self._init_fish(5)
        digit_int = int(digit) - 1
        if len(self._columns_to_check[5][digit_int]) > 4:
            idx1 = 0
            for col1 in self._columns_to_check[5][digit_int]:
                idx1 += 1
                idx2 = idx1
                for col2 in self._columns_to_check[5][digit_int][idx1:]:
                    idx2 += 1
                    idx3 = idx2
                    for col3 in self._columns_to_check[5][digit_int][idx2:]:
                        idx3 += 1
                        idx4 = idx3
                        for col4 in self._columns_to_check[5][digit_int][idx3:]:
                            idx4 += 1
                            for col5 in self._columns_to_check[5][digit_int][idx4:]:
                                pair_cnt = 0
                                y1 = -1
                                y2 = -1
                                y3 = -1
                                y4 = -1
                                y5 = -1
                                for row in range(9):
                                    if (self._row_column_counts[digit_int][row][col1] == 1 and
                                        self._row_column_counts[digit_int][row][col2] == 1) or \
                                            (self._row_column_counts[digit_int][row][col1] == 1 and
                                             self._row_column_counts[digit_int][row][col3] == 1) or \
                                            (self._row_column_counts[digit_int][row][col1] == 1 and
                                             self._row_column_counts[digit_int][row][col4] == 1) or \
                                            (self._row_column_counts[digit_int][row][col1] == 1 and
                                             self._row_column_counts[digit_int][row][col5] == 1) or \
                                            (self._row_column_counts[digit_int][row][col2] == 1 and
                                             self._row_column_counts[digit_int][row][col3] == 1) or \
                                            (self._row_column_counts[digit_int][row][col2] == 1 and
                                             self._row_column_counts[digit_int][row][col4] == 1) or \
                                            (self._row_column_counts[digit_int][row][col2] == 1 and
                                             self._row_column_counts[digit_int][row][col5] == 1) or \
                                            (self._row_column_counts[digit_int][row][col3] == 1 and
                                             self._row_column_counts[digit_int][row][col4] == 1) or \
                                            (self._row_column_counts[digit_int][row][col3] == 1 and
                                             self._row_column_counts[digit_int][row][col5] == 1) or \
                                            (self._row_column_counts[digit_int][row][col4] == 1 and
                                             self._row_column_counts[digit_int][row][col5] == 1):
                                        pair_cnt += 1
                                        if y1 == -1:
                                            y1 = row
                                        elif y2 == -1:
                                            y2 = row
                                        elif y3 == -1:
                                            y3 = row
                                        elif y4 == -1:
                                            y4 = row
                                        elif y5 == -1:
                                            y5 = row
                                    if self._row_column_counts[digit_int][row][col1] == -1 or \
                                            self._row_column_counts[digit_int][row][col2] == -1 or \
                                            self._row_column_counts[digit_int][row][col3] == -1 or \
                                            self._row_column_counts[digit_int][row][col4] == -1 or \
                                            self._row_column_counts[digit_int][row][col5] == -1:
                                        pair_cnt = 99
                                if pair_cnt == 5:
                                    found_outside = False
                                    for row in range(9):
                                        if row in (y1, y2, y3, y4, y5):
                                            continue
                                        if self._row_column_counts[digit_int][row][col1] == 1 or \
                                                self._row_column_counts[digit_int][row][col2] == 1 or \
                                                self._row_column_counts[digit_int][row][col3] == 1 or \
                                                self._row_column_counts[digit_int][row][col4] == 1 or \
                                                self._row_column_counts[digit_int][row][col5] == 1:
                                            found_outside = True
                                    if not found_outside:
                                        result.append((col1, y1))
                                        result.append((col2, y2))
                                        result.append((col3, y3))
                                        result.append((col4, y4))
                                        result.append((col5, y5))
                                if 5 < pair_cnt < 99:
                                    return []  # FEHLER
        return result

if __name__ == '__main__':
    import str8tssolver
    str8tssolver.main()
