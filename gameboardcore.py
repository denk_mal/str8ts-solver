#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=R1702,R0912,R0913,R0914,R0915

import itertools
from typing import Tuple, List

import gameboardgui


class GameboardCore(gameboardgui.GameboardGUI):
    _DIGITS_EXT = "123456789X"

    def sanity_check(self) -> int:
        """ check game state

        :return: -1 on error, 1 if solved. otherwise 0
        """
        resolved = True
        error = False

        # check for empty fields
        for y in range(9):
            for x in range(9):
                if not self._playground[x][y]:
                    if self._gui:
                        self._gui.set_label_text(x + 1, y + 1, "0", True, 0)
                    error = True
                    break
                if int(self._playground[x][y]) > 9:
                    resolved = False

        # check for doublicate digits in a column
        if not error:
            for x in range(9):
                digit_field = [[], [], [], [], [], [], [], [], []]
                for y in range(9):
                    digit = int(self._playground[x][y])
                    if 0 < digit < 10:
                        digit_field[digit - 1].append(y)
                for idx in range(9):
                    if len(digit_field[idx]) > 1:
                        error = True
                        if self._gui:
                            for yy in digit_field[idx]:
                                self._gui.set_label_text(x + 1, yy + 1, "0", True, 0)

        # check for doublicate digits in a row
        if not error:
            for y in range(9):
                digit_field = [[], [], [], [], [], [], [], [], []]
                for x in range(9):
                    digit = int(self._playground[x][y])
                    if 0 < digit < 10:
                        digit_field[digit - 1].append(x)
                for idx in range(9):
                    if len(digit_field[idx]) > 1:
                        error = True
                        if self._gui:
                            for xx in digit_field[idx]:
                                self._gui.set_label_text(xx + 1, y + 1, "0", True, 0)

        if not error:
            for y in range(9):
                streets = self.streets_row_low_high(y)
                for x1, x2 in streets:
                    complete_content = ""
                    for xx in range(x1, x2 + 1):
                        content = self.get_content(xx, y)
                        if len(content) > 1:
                            break
                        complete_content += content
                    if len(complete_content) == x2 - x1 + 1:
                        complete_content = "".join(sorted(set(complete_content)))
                        diff = int(complete_content[-1]) - int(complete_content[0])
                        if diff > x2 - x1:
                            if self._gui:
                                for xx in range(x1, x2 + 1):
                                    digit = self.get_content(xx, y)
                                    self._gui.set_label_text(xx + 1, y + 1, digit, True, 1)
                            error = True
            for x in range(9):
                streets = self.streets_column_low_high(x)
                for y1, y2 in streets:
                    complete_content = ""
                    for yy in range(y1, y2 + 1):
                        content = self.get_content(x, yy)
                        if len(content) > 1:
                            break
                        complete_content += content
                    if len(complete_content) == y2 - y1 + 1:
                        complete_content = "".join(sorted(set(complete_content)))
                        diff = int(complete_content[-1]) - int(complete_content[0])
                        if diff > y2 - y1:
                            if self._gui:
                                for yy in range(y1, y2 + 1):
                                    digit = self.get_content(x, yy)
                                    self._gui.set_label_text(x + 1, yy + 1, digit, True, 1)
                            error = True

        if error:
            return -1
        if resolved:
            return 1
        return 0

    def street_row_low_high(self, x: int, y: int) -> Tuple[int, int]:
        """ get the lower and upper row index for the row street that contains this field

        :param x: column
        :param y: row
        :return: lower and upper index of the street; (-1, -1) if not member of a street
        """
        streets = self.streets_row_low_high(y)

        for x1, x2 in streets:
            if x1 <= x <= x2:
                return x1, x2

        return -1, -1

    def street_column_low_high(self, x: int, y: int) -> Tuple[int, int]:
        """ get the lower and upper row index for the column street that contains this field

        :param x: column
        :param y: row
        :return: lower and upper index of the street; (-1, -1) if not member of a street
        """
        streets = self.streets_column_low_high(x)

        for y1, y2 in streets:
            if y1 <= y <= y2:
                return y1, y2

        return -1, -1

    def streets_row_low_high(self, y: int) -> List[Tuple[int, int]]:
        """ get all streets of a row

        :param y: row
        :return: array of lower and upper index of the streets
        """
        streets = []
        x = 0
        while x < 9:
            # find begin of the row street for the current cell
            x1 = x
            while x1 >= 0 and int(self._playground[x1][y]) > 0:
                x1 -= 1
            x1 += 1

            # find end of the row street for the current cell
            x2 = x1
            while x2 < 9 and int(self._playground[x2][y]) > 0:
                x2 += 1
            x2 -= 1

            if x1 <= x2:
                streets.append((x1, x2))
            x = x2 + 1

        return streets

    def streets_column_low_high(self, x: int) -> List[Tuple[int, int]]:
        """ get all streets of a column

        :param x: column
        :return: array of lower and upper index of the streets
        """
        streets = []
        y = 0
        while y < 9:
            # find begin of the row street for the current cell
            y1 = y
            while y1 >= 0 and int(self._playground[x][y1]) > 0:
                y1 -= 1
            y1 += 1

            # find end of the row street for the current cell
            y2 = y1
            while y2 < 9 and int(self._playground[x][y2]) > 0:
                y2 += 1
            y2 -= 1

            if y1 <= y2:
                streets.append((y1, y2))
            y = y2 + 1

        return streets

    def content_row(self, y: int) -> str:
        """ get fixed digits of a row

        :param y: row
        :return: all fixed digits in the row
        """
        content = ""
        for xx in range(9):
            digit = self.get_singledigit(xx, y)
            if digit != "0":
                content += digit
        return "".join(sorted(set(content)))

    def content_column(self, x: int) -> str:
        """ get fixed digits of a column

        :param x: row
        :return: all fixed digits in the column
        """
        content = ""
        for yy in range(9):
            digit = self.get_singledigit(x, yy)
            if digit != "0":
                content += digit
        return "".join(sorted(set(content)))

    def possible_row(self, x: int, y: int) -> str:
        """ get all possible digits incl. fixed digits of a street or all streets row

        :param x: column; -1 for all streets of a row
        :param y: row
        :return: all possible digits in this street (or whole row)
        """
        content = ""
        streets = self.streets_row_low_high(y)
        for x1, x2 in streets:
            content_street = ""
            if x1 <= x <= x2 or x == -1:
                if x1 != -1:
                    for xx in range(x1, x2 + 1):
                        content_street += self._playground[xx][y]
                    content_street = "".join(sorted(set(content_street)))

                    min_necessary = 9
                    max_necessary = 1
                    if int(content_street[0]) > max_necessary:
                        max_necessary = int(content_street[0])
                    if int(content_street[-1]) < min_necessary:
                        min_necessary = int(content_street[-1])

                    diff = x2 - x1
                    if diff:
                        min_necessary += diff
                        max_necessary -= diff
                    for digit in range(1, 10):
                        if digit < max_necessary or digit > min_necessary:
                            content_street = content_street.replace(str(digit), "")
                content += content_street

        content = "".join(sorted(set(content)))
        return content

    def possible_column(self, x: int, y: int) -> str:
        """ get all possible digits incl. fixed digits of a street or all streets column

        :param x: column
        :param y: row; -1 for all streets of a column
        :return: all possible digits in this street (or whole column)
        """
        content = ""
        streets = self.streets_column_low_high(x)
        for y1, y2 in streets:
            content_street = ""
            if y1 <= y <= y2 or y == -1:
                if y1 != -1:
                    for yy in range(y1, y2 + 1):
                        content_street += self._playground[x][yy]
                    content_street = "".join(sorted(set(content_street)))

                    min_necessary = 9
                    max_necessary = 1
                    if int(content_street[0]) > max_necessary:
                        max_necessary = int(content_street[0])
                    if int(content_street[-1]) < min_necessary:
                        min_necessary = int(content_street[-1])

                    diff = y2 - y1
                    if diff:
                        min_necessary += diff
                        max_necessary -= diff
                    for digit in range(1, 10):
                        if digit < max_necessary or digit > min_necessary:
                            content_street = content_street.replace(str(digit), "")
                content += content_street

        content = "".join(sorted(set(content)))
        return content

    def candidates_row(self, x: int, y: int) -> str:
        """ get all candidate digits excl. fixed digits of a street or all streets row

        :param x: column; -1 for all streets of a row
        :param y: row
        :return: all possible digits in this street (or whole row)
        """
        streets = self.streets_row_low_high(y)
        content_streets = []
        idx_found = -1
        for idx, street in enumerate(streets):
            content_street = ""
            x1, x2 = street
            if x1 <= x <= x2:
                idx_found = idx
            for xx in range(x1, x2 + 1):
                if len(self._playground[xx][y]) > 1:
                    content_street += self._playground[xx][y]

            content_street = "".join(sorted(set(content_street)))
            content_streets.append(content_street)

        if idx_found != -1:
            content = content_streets[idx_found]
        else:
            content = "".join(sorted(set("".join(content_streets))))

        return content

    def candidates_column(self, x: int, y: int) -> str:
        """ get all candidate digits excl. fixed digits of a street or all streets column

        :param x: column
        :param y: row; -1 for all streets of a column
        :return: all possible digits in this street (or whole column)
        """
        streets = self.streets_column_low_high(x)
        content_streets = []
        idx_found = -1
        for idx, street in enumerate(streets):
            content_street = ""
            y1, y2 = street
            if y1 <= y <= y2:
                idx_found = idx
            for yy in range(y1, y2 + 1):
                if len(self._playground[x][yy]) > 1:
                    content_street += self._playground[x][yy]

            content_street = "".join(sorted(set(content_street)))
            content_streets.append(content_street)

        if idx_found != -1:
            content = content_streets[idx_found]
        else:
            content = "".join(sorted(set("".join(content_streets))))

        return content

    def necessary_row(self, x: int, y: int) -> str:
        """ get all necessary (or sure) digits excl. fixed digits of a street or all streets row

        :param x: column; -1 for all streets of a row
        :param y: row
        :return: all necessary digits in this street (or whole row)
        """
        streets = self.streets_row_low_high(y)
        content_streets = []
        idx_found = -1
        for idx, street in enumerate(streets):
            content_street = ""
            x1, x2 = street
            if x1 <= x <= x2:
                idx_found = idx
            for xx in range(x1, x2 + 1):
                content_street += self._playground[xx][y]

            content_street = "".join(sorted(set(content_street)))
            diff = len(content_street) - x2 + x1 - 1
            if diff:
                content_street = content_street[diff:-diff]
            content_streets.append(content_street)

        if idx_found != -1:
            content = content_streets[idx_found]
        else:
            content = "".join(sorted(set("".join(content_streets))))

        for xx in range(9):
            if len(self._playground[xx][y]) == 1:
                content = content.replace(self._playground[xx][y], "")

        return content

    def necessary_column(self, x: int, y: int) -> str:
        """ get all necessary (or sure) digits excl. fixed digits of a street or all streets column

        :param x: column
        :param y: row; -1 for all streets of a column
        :return: all necessary digits in this street (or whole column)
        """
        streets = self.streets_column_low_high(x)
        content_streets = []
        idx_found = -1
        for idx, street in enumerate(streets):
            content_street = ""
            y1, y2 = street
            if y1 <= y <= y2:
                idx_found = idx
            for yy in range(y1, y2 + 1):
                content_street += self._playground[x][yy]

            content_street = "".join(sorted(set(content_street)))
            diff = len(content_street) - y2 + y1 - 1
            if diff:
                content_street = content_street[diff:-diff]
            content_streets.append(content_street)

        if idx_found != -1:
            content = content_streets[idx_found]
        else:
            content = "".join(sorted(set("".join(content_streets))))

        for yy in range(9):
            if len(self._playground[x][yy]) == 1:
                content = content.replace(self._playground[x][yy], "")

        return content

    def possible_row_low_high(self, x: int, y: int) -> Tuple[int, int]:
        """ get lowest and highest possible digit of a street in a row

        :param x: column
        :param y: row
        :return: the lowest and hightest possible digit
        """
        content = self.candidates_row(x, y)
        street_lower = int(content[0])
        street_upper = int(content[-1])

        x1, x2 = self.street_row_low_high(x, y)
        # find the lowest and highest fixed digit
        min_x = 9
        max_x = 1
        for xx in range(x1, x2 + 1):
            digit = int(self._playground[xx][y])
            if digit < min_x:
                min_x = digit
            if 10 > digit > max_x:
                max_x = digit

        # get lowest and highest possible digit
        lowest_x = max_x - (x2 - x1)  # (lowest fixed digit) - (street length)
        highest_x = min_x + (x2 - x1)  # (highest fixed digit) + (street length)
        lowest_x = max(lowest_x, street_lower)
        highest_x = min(highest_x, street_upper)

        return lowest_x, highest_x

    def possible_column_low_high(self, x: int, y: int) -> Tuple[int, int]:
        """ get lowest and highest possible digit of a street in a column

        :param x: column
        :param y: row
        :return: the lowest and hightest possible digit
        """
        content = self.possible_column(x, y)
        street_lower = int(content[0])
        street_upper = int(content[-1])

        y1, y2 = self.street_column_low_high(x, y)
        # find the lowest and highest fixed digit
        min_y = 9
        max_y = 1
        for yy in range(y1, y2 + 1):
            digit = int(self._playground[x][yy])
            if digit < min_y:
                min_y = digit
            if 10 > digit > max_y:
                max_y = digit

        # get lowest and highest possible digit
        lowest_y = max_y - (y2 - y1)  # (lowest fixed digit) - (street length)
        highest_y = min_y + (y2 - y1)  # (highest fixed digit) + (street length)
        lowest_y = max(lowest_y, street_lower)
        highest_y = min(highest_y, street_upper)

        return lowest_y, highest_y

    def necessary_row_low_high(self, x: int, y: int) -> Tuple[int, int]:
        """ get lowest and highest necessary digit of a street in a row

        :param x: column
        :param y: row
        :return: the lowest and hightest necessary digit
        """
        content = self.possible_row(x, y)
        street_lower = int(content[0])
        street_upper = int(content[-1])

        # get lowest and highest necessary digit
        x1, x2 = self.street_row_low_high(x, y)
        diff = x2 - x1
        lowest_x = street_upper - diff
        highest_x = street_lower + diff

        for xx in range(x1, x2 + 1):
            content = self.get_content(xx, y)
            if int(content[0]) > highest_x:
                highest_x = int(content[0])
            if int(content[-1]) < lowest_x:
                lowest_x = int(content[-1])

        return lowest_x, highest_x

    def necessary_column_low_high(self, x: int, y: int) -> Tuple[int, int]:
        """ get lowest and highest necessary digit of a street in a column

        :param x: column
        :param y: row
        :return: the lowest and hightest necessary digit
        """
        content = self.possible_column(x, y)
        street_lower = int(content[0])
        street_upper = int(content[-1])

        # get lowest and highest necessary digit
        y1, y2 = self.street_column_low_high(x, y)
        diff = y2 - y1
        lowest_y = street_upper - diff
        highest_y = street_lower + diff

        for yy in range(y1, y2 + 1):
            content = self.get_content(x, yy)
            if int(content[0]) > highest_y:
                highest_y = int(content[0])
            if int(content[-1]) < lowest_y:
                lowest_y = int(content[-1])

        return lowest_y, highest_y

    def is_unique_in_row_street(self, x: int, y: int, digit) -> bool:
        """ check if digit is a candidate in only one field in this row street

        :param x: column
        :param y: row
        :param digit: digit to search for
        :return: true if digit is a candidate in only one field
        """
        x1, x2 = self.street_row_low_high(x, y)
        cnt = 0
        for xx in range(x1, x2 + 1):
            if digit in self._playground[xx][y]:
                if len(self._playground[xx][y]) == 1:
                    return False
                cnt += 1
        return cnt == 1

    def is_unique_in_column_street(self, x: int, y: int, digit) -> bool:
        """ check if digit is a candidate in only one field in this column street

        :param x: column
        :param y: row
        :param digit: digit to search for
        :return: true if digit is a candidate in only one field
        """
        y1, y2 = self.street_column_low_high(x, y)
        cnt = 0
        for yy in range(y1, y2 + 1):
            if digit in self._playground[x][yy]:
                if len(self._playground[x][yy]) == 1:
                    return False
                cnt += 1
        return cnt == 1

    def streets_in_row(self, x: int, y: int):
        x1, x2 = self.street_row_low_high(x, y)
        diff = x2 - x1 + 1

        result = []
        possible = self.possible_row(x, y)
        if diff > 5:
            for idx in range(10 - diff):
                substreet = possible[idx:idx + diff]
                result.append(substreet)
            return result

        substreet = ""
        for digit in GameboardCore._DIGITS_EXT:
            if digit in possible:
                substreet += digit
                continue

            if not substreet:
                continue

            contents = []
            for xx in range(x1, x2 + 1):
                contents.append(self.get_content(xx, y, substreet))

            permutations = []
            gen = (''.join(sorted(p)) for p in itertools.product(*contents) if len(p) == len(set(p)))
            for entry in gen:
                if entry not in permutations:
                    permutations.append(entry)

            idx = 0
            while True:
                sub = GameboardCore._DIGITS[idx:idx + diff]
                if len(sub) < diff:
                    break
                if sub in permutations and sub not in result:
                    result.append(sub)
                idx += 1

            substreet = ""

        return result

    def streets_in_column(self, x: int, y: int):
        y1, y2 = self.street_column_low_high(x, y)
        diff = y2 - y1 + 1

        result = []
        possible = self.possible_column(x, y)
        if diff > 5:
            for idx in range(10 - diff):
                substreet = possible[idx:idx + diff]
                result.append(substreet)
            return result

        substreet = ""
        for digit in GameboardCore._DIGITS_EXT:
            if digit in possible:
                substreet += digit
                continue

            if not substreet:
                continue

            contents = []
            for yy in range(y1, y2 + 1):
                contents.append(self.get_content(x, yy, substreet))

            permutations = []
            gen = (''.join(sorted(p)) for p in itertools.product(*contents) if len(p) == len(set(p)))
            for entry in gen:
                if entry not in permutations:
                    permutations.append(entry)

            idx = 0
            while True:
                sub = GameboardCore._DIGITS[idx:idx + diff]
                if len(sub) < diff:
                    break
                if sub in permutations and sub not in result:
                    result.append(sub)
                idx += 1

            substreet = ""

        return result

    def streets_possible_in_row(self, x: int, y: int):
        x1, x2 = self.street_row_low_high(x, y)
        diff = x2 - x1 + 1

        result = []
        possible = self.possible_row(x, y)
        if diff > 5:
            result.append(possible)
            return result

        substreet = ""
        result = []
        for digit in GameboardCore._DIGITS_EXT:
            if digit in possible:
                substreet += digit
                continue

            if not substreet:
                continue

            result.append(substreet)

            substreet = ""

        return result

    def streets_possible_in_column(self, x: int, y: int):
        y1, y2 = self.street_column_low_high(x, y)
        diff = y2 - y1 + 1

        result = []
        possible = self.possible_column(x, y)
        if diff > 5:
            result.append(possible)
            return result

        substreet = ""
        result = []
        for digit in GameboardCore._DIGITS_EXT:
            if digit in possible:
                substreet += digit
                continue

            if not substreet:
                continue

            result.append(substreet)

            substreet = ""

        return result

if __name__ == '__main__':
    import str8tssolver
    str8tssolver.main()
