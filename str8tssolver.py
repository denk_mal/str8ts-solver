#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=C0302
# pylint: disable=R0902,R1702,R0912,R0913,R0914,R0915,R0916

"""Subclass of MainFrame, which translate standard functions to the gui framework (wxPython)."""

import json
import webbrowser
from typing import Callable, Dict, List, Tuple, Union

import wx

import gameboard
import str8tssolver_gui

class Str8tsSolver(str8tssolver_gui.Str8tsSolverGUI):
    DIGITS_EXT = "123456789X"

    def __init__(self, parent):
        super().__init__(parent)
        self.games: Dict[str, str] = {}
        self.solver_cb: List[Callable[[gameboard, int, int], str]] = []
        resultsizer = self.m_count_solved.GetContainingSizer()
        for child in resultsizer:
            widget = child.GetWindow()
            func_name = widget.GetName()
            if func_name and hasattr(self, func_name):
                funct = getattr(self, func_name)
                self.solver_cb.append(funct)
        self.solver_idx = 0
        self.count_changed = 0
        self.insert_guess: Dict[str, List[Tuple[int, int]]] = {}
        self.config: Dict[str, Union[int, bool, str]] = {}
        self.autorun = False
        self.autorun_file = False
        self.stop_on_success = True
        self.stop_on_failure = True
        self.singlestep = False
        self.try_guess_next = False
        self.result_txt = "Solved"

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.update, self.timer)

        self.m_static_text_autorun.SetLabel("")
        self.m_guess.Disable()

        self.load_config()
        self.read_gamefile()
        self.init_field()

    def init_field(self):
        super().init_field()
        gamename = self.m_select_game.GetString(self.m_select_game.GetSelection())
        if not gamename:
            return
        line = self.games[gamename]
        content_field = line[:81]
        mask_field = line[81:]
        self.solver_idx = 0
        self.count_changed = 0
        self.real_gameboard.init_field(content_field, mask_field)
        self.clear_result_labels()

    def clear_result_labels(self):
        super().clear_result_labels()
        self.real_gameboard.init_combined_environment()

    def read_gamefile(self):
        self.games = {}
        self.m_select_game.Clear()
        if 'gamefile' not in self.config or not self.config['gamefile']:
            return
        with open(self.config['gamefile'], encoding="utf8") as openfile:
            for line in openfile:
                line = line.strip()
                if not line or line[0] == '#':
                    continue
                *name, game = line.split()
                menuname = ' '.join(name)
                self.games[menuname] = game
                self.m_select_game.AppendItems(menuname)
        self.m_select_game.SetSelection(self.m_select_game.GetCount() - 1)

    def set_config(self, event):
        with str8tssolver_gui.ConfigDialogGUI(self, self.config) as dlg:
            if dlg.ShowModal() == wx.ID_SAVE:
                self.config['gamefile'] = dlg.m_file_picker.GetPath()
                self.config['autorun'] = dlg.m_autorun_single.GetValue()
                self.config['autorun_delay'] = dlg.m_slider_delay.GetValue()
                self.config['autorun_file'] = dlg.m_autorun_file.GetValue()
                self.config['stop_on_success'] = dlg.m_stop_on_success.GetValue()
                self.config['stop_on_failure'] = dlg.m_stop_on_failure.GetValue()
                self.config['singlestep'] = dlg.m_singlestep.GetValue()
                self.autorun_file = self.config['autorun_file']
                self.stop_on_success = self.config['stop_on_success']
                self.stop_on_failure = self.config['stop_on_failure']
                self.save_config()

    def load_config(self):
        """
        reload data and rebuild gui
        """
        config_file = ".settings"
        try:
            with open(config_file, encoding="utf8") as json_file:
                data = json.load(json_file)
                self.config = data
                self.autorun_file = self.config['autorun_file']
                self.stop_on_success = self.config['stop_on_success']
                self.stop_on_failure = self.config['stop_on_failure']
            if "singlestep" not in data:
                data["singlestep"] = False
        except FileNotFoundError:
            # wx.MessageBox("Could not load config file!", "Load Error")
            pass

    def save_config(self):
        """
        save the data into the configfile
        """
        try:
            with open(".settings", 'w', encoding="utf8") as json_file:
                json.dump(self.config, json_file, indent=2)
                self.read_gamefile()
                self.init_field()
        except FileNotFoundError:
            wx.MessageBox(message="Could not save config file!")
            return

    def save_game(self, event):
        if 'gamefile' not in self.config or not self.config['gamefile']:
            wx.MessageBox(message="No gamefile; Could not save game!")
            return
        dlg = wx.TextEntryDialog(self, "title for game to save")
        rc = dlg.ShowModal()
        if rc != wx.ID_OK:
            return
        title = dlg.GetValue()
        if not title:
            return
        with open(self.config['gamefile'], 'a', encoding="utf8") as openfile:
            content = self.real_gameboard.get_gameboard_string()
            new_line = f"{title} {content}\n"
            openfile.write(new_line)
        self.read_gamefile()

    def select_game(self, event):
        self.insert_guess = {}
        self.m_guess.Disable()
        self.init_field()

    def write_batchfile(self, state):
        gamename = self.m_select_game.GetString(self.m_select_game.GetSelection())
        if state < 0:
            result_txt = "ERROR"
        elif state > 0:
            result_txt = self.result_txt
        else:
            result_txt = "Unsolved"
        with open("results.txt", 'a', encoding="utf8") as openfile:
            new_line = f"Game: {gamename}; state = {result_txt}\n"
            openfile.write(new_line)

    def next_step(self, event):
        self.insert_guess = {}
        self.m_guess.Disable()
        ctrl_is_pressed = wx.GetKeyState(wx.WXK_CONTROL)
        shift_is_pressed = wx.GetKeyState(wx.WXK_SHIFT)
        if ctrl_is_pressed or (self.config['autorun'] and not self.autorun):
            self.m_static_text_autorun.SetLabel("Autorun active")
            self.m_static_text_autorun.SendSizeEventToParent()
            self.play_automatic()
        check_ok = False
        while not check_ok:
            self.real_gameboard.display_playground()
            self.set_result_text("")
            state = self.real_gameboard.sanity_check()
            if state:
                self.clear_result_labels()
                self.autorun = False
                if self.m_batchmode.GetValue():
                    self.write_batchfile(state)
                else:
                    if state < 0:
                        if self.stop_on_failure:
                            wx.MessageBox("An error occured.", "Error", wx.ICON_ERROR)
                            self.autorun_file = False
                    if state > 0 and self.stop_on_success:
                        wx.MessageBox("Thats all, you are ready.", "Ready", style=wx.ICON_INFORMATION)
                        self.autorun_file = False
                return
            self.count_changed = 0
            if not self.solver_idx:
                self.clear_result_labels()

            check_ok = self.call_solver(self.real_gameboard, self.solver_idx)

            if not check_ok:
                self.solver_idx += 1
            if self.solver_idx >= len(self.solver_cb):
                if self.try_guess_next:
                    self.guess_next(None)
                    self.guess_next(None)
                    self.solver_idx = 0
                    self.result_txt = "Solved (Guess)"
                else:
                    self.solver_idx = 0
                    self.autorun = False
                    self.m_guess.Enable()
                    if self.stop_on_failure:
                        if self.m_batchmode.GetValue():
                            self.write_batchfile(0)
                        else:
                            wx.MessageBox("Solver is running out of strategies.", "Info", wx.ICON_ERROR)
                            self.autorun_file = False
                    break
            if check_ok:
                self.set_resultstep(self.solver_idx + 1)
                if self.count_changed:
                    self.m_count_solved.SetLabel(str(self.count_changed))
                self.m_count_solved.SetForegroundColour(wx.Colour('GREEN'))
                self.solver_idx = 0
            else:
                self.m_count_solved.SetLabel("")
                self.m_count_solved.SetForegroundColour(wx.Colour('RED'))
            if shift_is_pressed or self.config['singlestep']:
                check_ok = True

    def play_automatic(self):
        self.autorun = True
        self.autorun_file = self.config['autorun_file']
        self.timer.Start(self.config['autorun_delay'])

    def update(self, event):
        if not self.autorun:
            self.timer.Stop()
            if self.autorun_file:
                self.result_txt = "Solved"
                idx = self.m_select_game.GetSelection() + 1
                if idx < self.m_select_game.GetCount():
                    self.m_select_game.SetSelection(idx)
                    self.init_field()
                    self.play_automatic()
                else:
                    self.m_static_text_autorun.SetLabel("")
            else:
                self.m_static_text_autorun.SetLabel("")
        else:
            self.next_step(event)

    def call_solver(self, board, idx):
        changed = False

        # create temp playground
        play_ground = []
        for y in range(9):
            play_ground.append([])
            for x in range(9):
                play_ground[y].append("")

        for y in range(9):
            for x in range(9):
                if not board.is_unsolved(x, y):
                    play_ground[x][y] = board.get_content(x, y)
                    continue

                # noinspection PyArgumentList
                content = self.solver_cb[idx](board, x, y)

                if content != board.get_content(x, y):
                    play_ground[x][y] = content
                    if board.has_gui():
                        self.set_label_text(x + 1, y + 1, content, False, idx + 1)
                    changed = True
                    if len(content) == 1:
                        self.count_changed += 1
                else:
                    play_ground[x][y] = board.get_content(x, y)

        if changed:
            for y in range(9):
                for x in range(9):
                    board.set_content(x, y, play_ground[x][y])

        if board.has_gui():
            self.set_result_label(idx, changed)
        return changed

    @staticmethod
    def possibles(board, x, y):
        content = board.get_content(x, y)

        # find fixed digits in the row and eliminate it from the current cell content
        for x1 in range(9):
            if x1 != x:
                cmp_value = board.get_singledigit(x1, y)
                content = content.replace(cmp_value, "")
        # find fixed digits in the column and eliminate it from the current cell content
        for y1 in range(9):
            if y1 != y:
                cmp_value = board.get_singledigit(x, y1)
                content = content.replace(cmp_value, "")

        return content

    @staticmethod
    def compartment(board, x, y):
        content = board.get_content(x, y)

        lowest_x, highest_x = board.possible_row_low_high(x, y)
        # eliminate every impossible digit
        for idx in range(1, lowest_x):
            content = content.replace(str(idx), "")
        for idx in range(highest_x + 1, 10):
            content = content.replace(str(idx), "")

        lowest_y, highest_y = board.possible_column_low_high(x, y)
        # eliminate every impossible digit
        for idx in range(1, lowest_y):
            content = content.replace(str(idx), "")
        for idx in range(highest_y + 1, 10):
            content = content.replace(str(idx), "")

        return content

    @staticmethod
    def mandatory_digit(board, x, y):
        content = board.get_content(x, y)

        streets = board.streets_row_low_high(y)
        for x1, x2 in streets:
            content_possible = board.possible_row(x1, y)
            content_candidate = board.candidates_row(x1, y)
            content_necessary = board.necessary_row(x1, y)
            if x1 <= x <= x2:
                for digit in content_candidate:
                    if digit in content_possible:
                        if board.is_unique_in_row_street(x, y, digit) and \
                                digit in content_necessary and digit in content:
                            board.add_result_text("Mandatory Digit (Row): at {at} can remove {content}",
                                                  content, digit, x, y)
                            return digit
                    else:
                        content_new = content.replace(digit, "")
                        board.add_result_text("Mandatory Digit (Row): at {at} can remove {content}",
                                              content, content_new, x, y)
                        content = content_new

            else:
                content_new = content
                for digit in content_necessary:
                    content_new = content_new.replace(digit, "")
                board.add_result_text("Mandatory Digit (Row): at {at} can remove {content}",
                                      content, content_new, x, y)
                content = content_new

        streets = board.streets_column_low_high(x)
        for y1, y2 in streets:
            content_possible = board.possible_column(x, y1)
            content_candidate = board.candidates_column(x, y1)
            content_necessary = board.necessary_column(x, y1)
            if y1 <= y <= y2:
                for digit in content_candidate:
                    if digit in content_possible:
                        if board.is_unique_in_column_street(x, y, digit) and \
                                digit in content_necessary and digit in content:
                            board.add_result_text("Mandatory Digit (Column): at {at} can remove {content}",
                                                  content, digit, x, y)
                            return digit
                    else:
                        content_new = content.replace(digit, "")
                        board.add_result_text("Mandatory Digit (Column): at {at} can remove {content}",
                                              content, content_new, x, y)
                        content = content_new

            else:
                content_new = content
                for digit in content_necessary:
                    content_new = content_new.replace(digit, "")
                board.add_result_text("Mandatory Digit (Column): at {at} can remove {content}",
                                      content, content_new, x, y)
                content = content_new

        return content

    @staticmethod
    def mandatory_seq(board, x, y):
        content = board.get_content(x, y)

        x1, x2 = board.street_row_low_high(x, y)
        streets = board.streets_possible_in_row(x, y)
        length = x2 - x1 + 1
        for substreet in streets:
            start = len(substreet) - length
            mandatory_digits = substreet[start:length]
            for mandatory_digit in mandatory_digits:
                if board.is_unique_in_row_street(x, y, mandatory_digit):
                    if mandatory_digit in content:
                        cmp_value = mandatory_digits.replace(mandatory_digit, "")
                        content_new = content
                        for digit in cmp_value:
                            content_new = content_new.replace(digit, "")
                        board.add_result_text("Mandatory Sequence (Row): at {at} can remove {content}",
                                              content, content_new, x, y)
                        content = content_new

        y1, y2 = board.street_column_low_high(x, y)
        streets = board.streets_possible_in_column(x, y)
        length = y2 - y1 + 1
        for substreet in streets:
            start = len(substreet) - length
            mandatory_digits = substreet[start:length]
            for mandatory_digit in mandatory_digits:
                if board.is_unique_in_column_street(x, y, mandatory_digit):
                    if mandatory_digit in content:
                        cmp_value = mandatory_digits.replace(mandatory_digit, "")
                        content_new = content
                        for digit in cmp_value:
                            content_new = content_new.replace(digit, "")
                        board.add_result_text("Mandatory Sequence (Column): at {at} can remove {content}",
                                              content, content_new, x, y)
                        content = content_new

        return content

    @staticmethod
    def stranded_digit(board, x, y):
        content = board.get_content(x, y)

        x1, x2 = board.street_row_low_high(x, y)
        digit_field = [[], [], [], [], [], [], [], [], [], []]
        diff = x2 - x1
        if diff > 0:
            for xx in range(x1, x2 + 1):
                if xx == x:
                    continue
                fld_content = board.get_content(xx, y)
                for digit in fld_content:
                    digit_field[int(digit) - 1].append(xx)

            content_new = content
            for digit in content_new:
                digit_int = int(digit) - 1
                if not digit_field[digit_int + 1] and \
                        (not digit_int or not digit_field[digit_int - 1]):
                    content_new = content_new.replace(digit, "")

            board.add_result_text("Stranded Digit (Row): at {at} can remove {content}",
                                  content, content_new, x, y)
            content = content_new

        y1, y2 = board.street_column_low_high(x, y)
        digit_field = [[], [], [], [], [], [], [], [], [], []]
        diff = y2 - y1
        if diff > 0:
            for yy in range(y1, y2 + 1):
                if yy == y:
                    continue
                fld_content = board.get_content(x, yy)
                for digit in fld_content:
                    digit_field[int(digit) - 1].append(yy)

            content_new = content
            for digit in content_new:
                digit_int = int(digit) - 1
                if not digit_field[digit_int + 1] and \
                        (not digit_int or not digit_field[digit_int - 1]):
                    content_new = content_new.replace(digit, "")

            board.add_result_text("Stranded Digit (Column): at {at} can remove {content}",
                                  content, content_new, x, y)
            content = content_new

        return content

    @staticmethod
    def stranded_seq(board, x, y):
        content = board.get_content(x, y)

        x1, x2 = board.street_row_low_high(x, y)
        digit_field = [[], [], [], [], [], [], [], [], [], []]
        diff = x2 - x1 + 1
        if diff > 1:
            for xx in range(x1, x2 + 1):
                fld_content = board.get_content(xx, y)
                for digit in fld_content:
                    digit_field[int(digit) - 1].append(xx)

            lowerbound = 0
            content_new = content
            while True:
                while not digit_field[lowerbound]:
                    if lowerbound == 9:
                        break
                    lowerbound += 1
                upperbound = lowerbound
                while digit_field[upperbound]:
                    if upperbound == 9:
                        break
                    upperbound += 1

                if (upperbound - lowerbound) < diff:
                    for d in range(lowerbound, upperbound):
                        content_new = content_new.replace(str(d + 1), "")

                lowerbound = upperbound
                upperbound += 1
                if upperbound > 8:
                    break
            board.add_result_text("Stranded Sequence (Row): at {at} can remove {content}",
                                  content, content_new, x, y)
            content = content_new

        y1, y2 = board.street_column_low_high(x, y)
        digit_field = [[], [], [], [], [], [], [], [], [], []]
        diff = y2 - y1 + 1
        if diff > 1:
            for yy in range(y1, y2 + 1):
                fld_content = board.get_content(x, yy)
                for digit in fld_content:
                    digit_field[int(digit) - 1].append(yy)

            lowerbound = 0
            content_new = content
            while True:
                while not digit_field[lowerbound]:
                    if lowerbound == 9:
                        break
                    lowerbound += 1
                upperbound = lowerbound
                while digit_field[upperbound]:
                    if upperbound == 9:
                        break
                    upperbound += 1

                if (upperbound - lowerbound) < diff:
                    for d in range(lowerbound, upperbound):
                        content_new = content_new.replace(str(d + 1), "")

                lowerbound = upperbound
                upperbound += 1
                if upperbound > 8:
                    break
            board.add_result_text("Stranded Sequence (Column): at {at} can remove {content}",
                                  content, content_new, x, y)
            content = content_new

        return content

    @staticmethod
    def high_low_gap(board, x, y):
        content = board.get_content(x, y)

        x1, x2 = board.street_row_low_high(x, y)
        diff = x2 - x1
        if diff + 1 >= len(content):
            content1 = board.possible_row(x, y)
            street_lower = int(content1[0])
            street_upper = int(content1[-1])

            if street_upper - street_lower > diff:
                for xx in range(x1, x2 + 1):
                    if xx == x:
                        continue
                    fld_content = board.get_content(xx, y)
                    if len(fld_content) == 2:
                        lowerbound = int(fld_content[0])
                        upperbound = int(fld_content[1])
                        if lowerbound <= street_lower and upperbound >= street_upper:
                            content_new = content.replace(str(lowerbound), "")
                            content_new = content_new.replace(str(upperbound), "")
                            board.add_result_text("High Low Gap (Row): at {at} can remove {content}",
                                                  content, content_new, x, y)
                            content = content_new

        y1, y2 = board.street_column_low_high(x, y)
        diff = y2 - y1
        if diff + 1 > len(content):
            content1 = board.possible_column(x, y)
            street_lower = int(content1[0])
            street_upper = int(content1[-1])

            if street_upper - street_lower > diff:
                for yy in range(y1, y2 + 1):
                    if yy == y:
                        continue
                    fld_content = board.get_content(x, yy)
                    if len(fld_content) == 2:
                        lowerbound = int(fld_content[0])
                        upperbound = int(fld_content[1])
                        if lowerbound <= street_lower and upperbound >= street_upper:
                            content_new = content.replace(str(lowerbound), "")
                            content_new = content_new.replace(str(upperbound), "")
                            board.add_result_text("High Low Gap (Column): at {at} can remove {content}",
                                                  content, content_new, x, y)
                            content = content_new

        return content

    @staticmethod
    def isolated_digit(board, x, y):
        content = board.get_content(x, y)

        x1, x2 = board.street_row_low_high(x, y)
        diff = x2 - x1 + 1
        if diff > 1:
            possible = board.possible_row(x, y)

            obsolete = ""
            part_content = ""
            for digit in Str8tsSolver.DIGITS_EXT:
                if digit in possible:
                    part_content += digit
                else:
                    if len(part_content) < diff:
                        obsolete += part_content
                    part_content = ""

            content_new = content
            # eliminate every impossible digit
            for digit in obsolete:
                content_new = content_new.replace(digit, "")
            board.add_result_text("Isolated Digit (Row): at {at} can remove {content}",
                                  content, content_new, x, y)
            content = content_new

        y1, y2 = board.street_column_low_high(x, y)
        diff = y2 - y1 + 1
        if diff > 1:
            possible = board.possible_column(x, y)

            obsolete = ""
            part_content = ""
            for digit in Str8tsSolver.DIGITS_EXT:
                if digit in possible:
                    part_content += digit
                else:
                    if len(part_content) < diff:
                        obsolete += part_content
                    part_content = ""

            content_new = content
            # eliminate every impossible digit
            for digit in obsolete:
                content_new = content_new.replace(digit, "")
            board.add_result_text("Isolated Digit (Column): at {at} can remove {content}",
                                  content, content_new, x, y)
            content = content_new

        return content

    @staticmethod
    def street_range(board, x, y):
        content = board.get_content(x, y)

        streets = board.streets_row_low_high(y)
        for x1, x2 in streets:
            diff = x2 - x1 + 1
            low_high = ""
            if x1 <= x <= x2:
                for xx in range(x1, x2 + 1):
                    if xx == x:
                        continue
                    possible = board.get_content(xx, y)
                    if len(possible) == 2 and int(possible[-1]) - int(possible[0]) == diff:
                        low_high = possible
            else:
                lower, upper = board.necessary_row_low_high(x1, y)
                for idx in range(lower, upper + 1):
                    low_high += str(idx)

            content_new = content
            for digit in low_high:
                content_new = content_new.replace(digit, "")
            board.add_result_text("Street (Row): '{obj}' can remove {content} at {at}",
                                  content, content_new, x, y, low_high)
            if content != content_new:
                content = content_new

        streets = board.streets_column_low_high(x)
        for y1, y2 in streets:
            diff = y2 - y1 + 1
            low_high = ""
            if y1 <= y <= y2:
                for yy in range(y1, y2 + 1):
                    if yy == y:
                        continue
                    possible = board.get_content(x, yy)
                    if len(possible) == 2 and int(possible[-1]) - int(possible[0]) == diff:
                        low_high = possible
            else:
                lower, upper = board.necessary_column_low_high(x, y1)
                for idx in range(lower, upper + 1):
                    low_high += str(idx)

            content_new = content
            for digit in low_high:
                content_new = content_new.replace(digit, "")
            board.add_result_text("Street (Column): '{obj}' can remove {content} at {at}",
                                  content, content_new, x, y, low_high)
            if content != content_new:
                content = content_new

        return content

    @staticmethod
    def required_digit(board, x, y):
        content = board.get_content(x, y)

        x1, x2 = board.street_row_low_high(x, y)
        diff = x1 - x1 + 1
        possible = board.possible_row(x, y)
        if 1 < diff < len(possible):
            substreet = ""
            for digit in Str8tsSolver.DIGITS_EXT:
                if digit in possible:
                    substreet += digit
                    continue
                if diff == len(substreet) and diff < len(possible):
                    singledigit = ''
                    for digit2 in substreet:
                        if board.is_unique_in_row_street(x, y, digit2):
                            singledigit = digit2
                    if singledigit and singledigit in content:
                        content_new = content
                        for char in substreet:
                            if char != singledigit:
                                content_new = content_new.replace(char, "")
                        board.add_result_text("Required Digit (Row): '{obj}' can remove {content} at {at}",
                                              content, content_new, x, y, substreet)
                        content = content_new
                        substreet = ""
                        continue
                    for xx in range(x1, x2 + 1):
                        t1 = board.get_content(xx, y, substreet)
                        singledigit = ''
                        if len(t1) == 1 and xx != x:
                            singledigit = t1
                        if singledigit and singledigit in content:
                            content_new = content.replace(singledigit, "")
                            board.add_result_text("Required Digit (Row): '{obj}' can remove {content} at {at}",
                                                  content, content_new, x, y, substreet)
                            content = content_new
                substreet = ""

        y1, y2 = board.street_column_low_high(x, y)
        diff = y2 - y1 + 1
        possible = board.possible_column(x, y)
        if 1 < diff < len(possible):
            substreet = ""
            for digit in Str8tsSolver.DIGITS_EXT:
                if digit in possible:
                    substreet += digit
                    continue
                if diff == len(substreet) and diff < len(possible):
                    singledigit = ''
                    for digit2 in substreet:
                        if board.is_unique_in_column_street(x, y, digit2):
                            singledigit = digit2
                    if singledigit and singledigit in content:
                        content_new = content
                        for char in substreet:
                            if char != singledigit:
                                content_new = content_new.replace(char, "")
                        board.add_result_text("Required Digit (Column): '{obj}' can remove {content} at {at}",
                                              content, content_new, x, y, substreet)
                        content = content_new
                        substreet = ""
                        continue
                    for yy in range(y1, y2 + 1):
                        t1 = board.get_content(x, yy, substreet)
                        singledigit = ''
                        if len(t1) == 1 and yy != y:
                            singledigit = t1
                        if singledigit and singledigit in content:
                            content_new = content.replace(singledigit, "")
                            board.add_result_text("Required Digit (Column): '{obj}' can remove {content} at {at}",
                                                  content, content_new, x, y, substreet)
                            content = content_new
                substreet = ""

        return content

    @staticmethod
    def naked_pair(board, x, y):
        content = board.get_content(x, y)

        pairs = []
        for xx in range(9):
            if xx == x:
                continue
            fld_content = board.get_content(xx, y)
            if len(fld_content) == 2 and int(fld_content) > 10:
                pairs.append(fld_content)

        pair_candidates = []
        pairs_found = []
        for item in pairs:
            if item in pair_candidates:
                pairs_found.append(item)
            else:
                pair_candidates.append(item)

        for pair in pairs_found:
            if content != pair:
                content_new = content
                for digit in pair:
                    content_new = content_new.replace(digit, "")
                board.add_result_text("Naked Pair (Row): '{obj}' can remove {content} at {at}",
                                      content, content_new, x, y, pair)
                content = content_new

        pairs = []
        for yy in range(9):
            if yy == y:
                continue
            fld_content = board.get_content(x, yy)
            if len(fld_content) == 2 and int(fld_content) > 10:
                pairs.append(fld_content)

        pair_candidates = []
        pairs_found = []
        for item in pairs:
            if item in pair_candidates:
                pairs_found.append(item)
            else:
                pair_candidates.append(item)

        for pair in pairs_found:
            if content != pair:
                content_new = content
                for digit in pair:
                    content_new = content_new.replace(digit, "")
                board.add_result_text("Naked Pair (Column): '{obj}' can remove {content} at {at}",
                                      content, content_new, x, y, pair)
                content = content_new

        return content

    @staticmethod
    def naked_triple(board, x, y):
        content = board.get_content(x, y)

        triples = []
        for xx in range(9):
            if xx == x:
                continue
            fld_content = board.get_content(xx, y)
            if len(fld_content) == 2 or len(fld_content) == 3:
                triples.append(fld_content)

        triples_found = []
        for xx in range(len(Str8tsSolver.DIGITS_EXT) - 3):
            for yy in range(xx + 1, len(Str8tsSolver.DIGITS_EXT) - 2):
                for zz in range(yy + 1, len(Str8tsSolver.DIGITS_EXT) - 1):
                    tg = f"{Str8tsSolver.DIGITS_EXT[xx]}{Str8tsSolver.DIGITS_EXT[yy]}{Str8tsSolver.DIGITS_EXT[zz]}"
                    t1 = f"{Str8tsSolver.DIGITS_EXT[xx]}{Str8tsSolver.DIGITS_EXT[yy]}"
                    t2 = f"{Str8tsSolver.DIGITS_EXT[xx]}{Str8tsSolver.DIGITS_EXT[zz]}"
                    t3 = f"{Str8tsSolver.DIGITS_EXT[yy]}{Str8tsSolver.DIGITS_EXT[zz]}"
                    cnt = 0
                    for fld in triples:
                        if fld in (tg, t1, t2, t3):
                            cnt += 1
                    if cnt == 3:
                        triples_found.append(tg)

        for triple in triples_found:
            if content != triple:
                content_new = content
                for digit in triple:
                    content_new = content_new.replace(digit, "")
                board.add_result_text("Naked Triple (Row): '{obj}' can remove {content} at {at}",
                                      content, content_new, x, y, triple)
                content = content_new

        triples = []
        for yy in range(9):
            if yy == y:
                continue
            fld_content = board.get_content(x, yy)
            if len(fld_content) == 2 or len(fld_content) == 3:
                triples.append(fld_content)

        triples_found = []
        for xx in range(len(Str8tsSolver.DIGITS_EXT) - 3):
            for yy in range(xx + 1, len(Str8tsSolver.DIGITS_EXT) - 2):
                for zz in range(yy + 1, len(Str8tsSolver.DIGITS_EXT) - 1):
                    tg = f"{Str8tsSolver.DIGITS_EXT[xx]}{Str8tsSolver.DIGITS_EXT[yy]}{Str8tsSolver.DIGITS_EXT[zz]}"
                    t1 = f"{Str8tsSolver.DIGITS_EXT[xx]}{Str8tsSolver.DIGITS_EXT[yy]}"
                    t2 = f"{Str8tsSolver.DIGITS_EXT[xx]}{Str8tsSolver.DIGITS_EXT[zz]}"
                    t3 = f"{Str8tsSolver.DIGITS_EXT[yy]}{Str8tsSolver.DIGITS_EXT[zz]}"
                    cnt = 0
                    for fld in triples:
                        if fld in (tg, t1, t2, t3):
                            cnt += 1
                    if cnt == 3:
                        triples_found.append(tg)

        for triple in triples_found:
            if content != triple:
                content_new = content
                for digit in triple:
                    content_new = content_new.replace(digit, "")
                board.add_result_text("Naked Triple (Column): '{obj}' can remove {content} at {at}",
                                      content, content_new, x, y, triple)
                content = content_new

        return content

    @staticmethod
    def compartment_high_low(board, x, y):
        content = board.get_content(x, y)

        combinations_tocheck = board.streets_in_row(x, y)
        x1, x2 = board.street_row_low_high(x, y)
        combinations = []
        for combination in combinations_tocheck:
            remove = False
            for xx in range(x1, x2+1):
                content_field = board.get_content(xx, y)
                if combination[-1] < content_field[0] or combination[0] > content_field[-1]:
                    remove = True
                    break
            if not remove:
                combinations.append(combination)
        allowed_digits = ''.join(sorted(set(''.join(combinations))))

        candidates = ""
        streets = board.streets_row_low_high(y)
        cnt_free_fields = 0
        for x1, x2 in streets:
            if x1 <= x <= x2:
                continue

            for xx in range(x1, x2 + 1):
                candidates += board.candidates_row(xx, y)
                if board.is_unsolved(xx, y):
                    cnt_free_fields += 1
        candidates = ''.join(sorted(set(''.join(candidates))))
        if len(candidates) == cnt_free_fields:
            for digit in candidates:
                allowed_digits = allowed_digits.replace(digit, "")

        if allowed_digits:
            content_new = content
            for digit in content:
                if digit not in allowed_digits:
                    content_new = content_new.replace(digit, "")
            board.add_result_text("Compartement High Low (Row): '{obj}' can remove {content} at {at}",
                                  content, content_new, x, y, allowed_digits)
            if content != content_new:
                content = content_new

        combinations_tocheck = board.streets_in_column(x, y)
        y1, y2 = board.street_column_low_high(x, y)
        combinations = []
        for combination in combinations_tocheck:
            remove = False
            for yy in range(y1, y2+1):
                content_field = board.get_content(x, yy)
                if combination[-1] < content_field[0] or combination[0] > content_field[-1]:
                    remove = True
                    break
            if not remove:
                combinations.append(combination)
        allowed_digits = ''.join(sorted(set(''.join(combinations))))

        candidates = ""
        streets = board.streets_column_low_high(x)
        cnt_free_fields = 0
        for y1, y2 in streets:
            if y1 <= y <= y2:
                continue

            for yy in range(y1, y2 + 1):
                candidates += board.candidates_row(x, yy)
                if board.is_unsolved(x, yy):
                    cnt_free_fields += 1
        candidates = ''.join(sorted(set(''.join(candidates))))
        if len(candidates) == cnt_free_fields:
            for digit in candidates:
                allowed_digits = allowed_digits.replace(digit, "")

        if allowed_digits:
            content_new = content
            for digit in content:
                if digit not in allowed_digits:
                    content_new = content_new.replace(digit, "")
            board.add_result_text("Compartement High Low (Column): '{obj}' can remove {content} at {at}",
                                  content, content_new, x, y, allowed_digits)
            if content != content_new:
                content = content_new

        return content

    @staticmethod
    def range(board, x, y):
        content = board.get_content(x, y)

        combinations = board.streets_in_row(x, y)
        allowed_digits = ''.join(sorted(set(''.join(combinations))))

        streets = board.streets_row_low_high(y)
        lower_street, upper_street = board.necessary_row_low_high(x, y)
        for x1, x2 in streets:
            if x1 <= x <= x2:
                continue
            if lower_street <= upper_street:
                lower, upper = board.necessary_row_low_high(x1, y)
                if x1 == x2:
                    if lower > upper_street and upper < lower_street:
                        continue
                if lower <= upper:
                    if upper < lower_street:
                        for digit in range(1, upper + 1):
                            allowed_digits = allowed_digits.replace(str(digit), "")
                    if lower > upper_street:
                        for digit in range(lower, 10):
                            allowed_digits = allowed_digits.replace(str(digit), "")

        if allowed_digits:
            content_new = content
            for digit in content:
                if digit not in allowed_digits:
                    content_new = content_new.replace(digit, "")
            board.add_result_text("Range (Row): '{obj}' can remove {content} at {at}",
                                  content, content_new, x, y, allowed_digits)
            if content != content_new:
                content = content_new

        combinations = board.streets_in_column(x, y)
        allowed_digits = ''.join(sorted(set(''.join(combinations))))

        streets = board.streets_column_low_high(x)
        lower_street, upper_street = board.necessary_column_low_high(x, y)
        for y1, y2 in streets:
            if y1 <= y <= y2:
                continue
            lower, upper = board.necessary_column_low_high(x, y1)
            if y1 == y2:
                if lower > upper_street and upper < lower_street:
                    continue
            if lower_street <= upper_street:
                if lower <= upper or y2 == y1:
                    if upper < lower_street:
                        for digit in range(1, upper + 1):
                            allowed_digits = allowed_digits.replace(str(digit), "")
                    if lower > upper_street:
                        for digit in range(lower, 10):
                            allowed_digits = allowed_digits.replace(str(digit), "")

        if allowed_digits:
            content_new = content
            for digit in content:
                if digit not in allowed_digits:
                    content_new = content_new.replace(digit, "")
            board.add_result_text("Range (Column): '{obj}' can remove {content} at {at}",
                                  content, content_new, x, y, allowed_digits)
            if content != content_new:
                content = content_new

        return content

    @staticmethod
    def x_wing(board, x, y):
        content = board.get_content(x, y)

        content_new = content
        for digit in content:
            result = board.x_wing_row_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                if x in (x1, x2) and y not in (y1, y2):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y1),
                                             board.convert_coords_to_gui(x1, y2),
                                             board.convert_coords_to_gui(x2, y2)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("X-Wing (Row): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
            result = board.x_wing_column_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                if y in (y1, y2) and x not in (x1, x2):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y1),
                                             board.convert_coords_to_gui(x1, y2),
                                             board.convert_coords_to_gui(x2, y2)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("X-Wing (Column): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
        content = content_new

        return content

    @staticmethod
    def swordfish(board, x, y):
        content = board.get_content(x, y)

        content_new = content
        for digit in content:
            result = board.swordfish_row_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                x3, y3 = result.pop(0)
                if x in (x1, x2, x3) and y not in (y1, y2, y3):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y2),
                                             board.convert_coords_to_gui(x3, y3)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Swordfish (Row): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
            result = board.swordfish_column_coords(digit)
            if result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                x3, y3 = result.pop(0)
                if y in (y1, y2, y3) and x not in (x1, x2, x3):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y2),
                                             board.convert_coords_to_gui(x3, y3)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Swordfish (Column): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
        content = content_new

        return content

    @staticmethod
    def jellyfish(board, x, y):
        content = board.get_content(x, y)

        content_new = content
        for digit in content:
            result = board.jellyfish_row_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                x3, y3 = result.pop(0)
                x4, y4 = result.pop(0)
                if x in (x1, x2, x3, x4) and y not in (y1, y2, y3, y4):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y2),
                                             board.convert_coords_to_gui(x3, y3),
                                             board.convert_coords_to_gui(x4, y4)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Jellyfish (Row): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
            result = board.jellyfish_column_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                x3, y3 = result.pop(0)
                x4, y4 = result.pop(0)
                if y in (y1, y2, y3, y4) and x not in (x1, x2, x3, x4):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y2),
                                             board.convert_coords_to_gui(x3, y3),
                                             board.convert_coords_to_gui(x4, y4)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Jellyfish (Column): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
        content = content_new

        return content

    @staticmethod
    def starfish(board, x, y):
        content = board.get_content(x, y)

        content_new = content
        for digit in content:
            result = board.starfish_row_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                x3, y3 = result.pop(0)
                x4, y4 = result.pop(0)
                x5, y5 = result.pop(0)
                if x in (x1, x2, x3, x4, x5) and y not in (y1, y2, y3, y4, y5):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y2),
                                             board.convert_coords_to_gui(x3, y3),
                                             board.convert_coords_to_gui(x4, y4),
                                             board.convert_coords_to_gui(x5, y5)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Starfish (Row): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
            result = board.starfish_column_coords(digit)
            while result:
                x1, y1 = result.pop(0)
                x2, y2 = result.pop(0)
                x3, y3 = result.pop(0)
                x4, y4 = result.pop(0)
                x5, y5 = result.pop(0)
                if y in (y1, y2, y3, y4, y5) and x not in (x1, x2, x3, x4, x5):
                    board_coords = ",".join([board.convert_coords_to_gui(x1, y1),
                                             board.convert_coords_to_gui(x2, y2),
                                             board.convert_coords_to_gui(x3, y3),
                                             board.convert_coords_to_gui(x4, y4),
                                             board.convert_coords_to_gui(x5, y5)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Starfish (Column): {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
        content = content_new

        return content

    @staticmethod
    def setti(board, x, y):
        content = board.get_content(x, y)

        cnt_row_n = [0, 0, 0, 0, 0, 0, 0, 0, 0]  # digit 1-9 (y)
        cnt_row_c = [0, 0, 0, 0, 0, 0, 0, 0, 0]  # digit 1-9 (y)
        for yy in range(9):
            content_n = board.necessary_row(-1, yy)
            content_c = board.candidates_row(-1, yy)

            for digit in content_n:
                cnt_row_n[int(digit) - 1] += 1
            for digit in content_c:
                cnt_row_c[int(digit) - 1] += 1

        cnt_col_n = [0, 0, 0, 0, 0, 0, 0, 0, 0]  # digit 1-9 (x)
        cnt_col_c = [0, 0, 0, 0, 0, 0, 0, 0, 0]  # digit 1-9 (x)
        for xx in range(9):
            content_n = board.necessary_column(xx, -1)
            content_c = board.candidates_column(xx, -1)

            for digit in content_n:
                cnt_col_n[int(digit) - 1] += 1
            for digit in content_c:
                cnt_col_c[int(digit) - 1] += 1

        content_new = content
        for digit in content:
            idx = int(digit) - 1
            if cnt_row_n[idx] == cnt_col_c[idx]:
                content_n = board.necessary_row(-1, y)
                content_c = board.candidates_row(-1, y)
                if digit in content_c and digit not in content_n:
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Setti (Row): can remove {content} at {at}", content, content_new, x, y)
                    content = content_new

            if cnt_col_n[idx] == cnt_row_c[idx]:
                content_n = board.necessary_column(x, -1)
                content_c = board.candidates_column(x, -1)
                if digit in content_c and digit not in content_n:
                    content_new = content_new.replace(digit, "")
                    board.add_result_text("Setti (Column): can remove {content} at {at}", content, content_new, x, y)
                    content = content_new

        return content

    @staticmethod
    def xyz(board, x, y):
        content = board.get_content(x, y)

        for digit in content:
            for xx in range(9):
                if xx == x:
                    continue
                content_column = board.get_content(xx, y)
                if digit not in content_column or len(content_column) != 2:
                    continue
                for yy in range(9):
                    if yy == y:
                        continue
                    content_row = board.get_content(x, yy)
                    if digit not in content_row or len(content_row) != 2:
                        continue
                    content_row_column = board.get_content(xx, yy)
                    if len(content_row_column) != 2:
                        continue
                    digit_column = content_column.replace(digit, "")
                    digit_row = content_row.replace(digit, "")
                    if digit_row not in content_row_column or digit_column not in content_row_column:
                        continue
                    if digit_row == digit_column and digit not in content_row_column:
                        continue
                    board_coords = ",".join([board.convert_coords_to_gui(xx, yy),
                                             board.convert_coords_to_gui(xx, y),
                                             board.convert_coords_to_gui(x, yy),
                                             board.convert_coords_to_gui(x, y)])
                    coords = f"{digit}:({board_coords})"
                    content_new = content.replace(digit, "")
                    board.add_result_text("X-Y-Z: {obj} can remove {content} at {at}", content,
                                          content_new, x, y, coords)
                    if content != content_new:
                        content = content_new

        return content

    def check_validity(self, board):
        solver_idx = 0
        state = 0

        while not state:
            check_ok = self.call_solver(board, solver_idx)
            if check_ok:
                solver_idx = 0
            else:
                solver_idx += 1
            state = board.sanity_check()
            if solver_idx >= len(self.solver_cb):
                break
        return state

    def guess_next(self, event):
        if self.insert_guess:  # set already calculate guess into real gameboard
            for key, value in self.insert_guess.items():
                if key[-1] == "w":
                    digit = key[0]
                    for x, y in value:
                        self.real_gameboard.set_content(x, y, digit)
                        self.set_label_text(x + 1, y + 1, digit, False, 99)
                        self.insert_guess = {}
                    return

            for key, value in self.insert_guess.items():
                digit = key[0]
                for x, y in value:
                    content = self.real_gameboard.get_content(x, y)
                    content = content.replace(digit, "")
                    self.real_gameboard.set_content(x, y, content)
                    self.set_label_text(x + 1, y + 1, content, False, 99)

            self.insert_guess = {}
            self.m_guess.Disable()
            return

        self.m_panel_toolbar.Disable()
        self.m_take_step.Disable()
        self.m_gauge_process.Show()
        self.m_gauge_process.SendSizeEventToParent()
        self.m_gauge_process.Pulse()
        self.guess_worker()

    def display_worker_text(self, msg, task_ready=False):
        self.m_static_text_autorun.SetLabel(msg)
        self.m_static_text_autorun.SendSizeEventToParent()
        self.m_gauge_process.Pulse()
        if task_ready:
            self.m_gauge_process.Hide()
            self.m_gauge_process.SendSizeEventToParent()
            self.m_panel_toolbar.Enable()
            self.m_take_step.Enable()

    def guess_worker(self):
        candidates_found = False
        for length in range(2, 8):
            self.display_worker_text(f"Guess for '{length}'")
            for y in range(9):
                for x in range(9):
                    content = self.real_gameboard.get_content(x, y)
                    if len(content) == length and int(content) > 0:
                        for digit in content:
                            test_gameboard = gameboard.Gameboard(None)
                            # data = self.real_gameboard.get_gameboard_string()
                            # test_gameboard.init_field(data[:81], data[81:])
                            for xx in range(9):
                                for yy in range(9):
                                    test_gameboard.set_content(xx, yy, self.real_gameboard.get_content(xx, yy))
                            test_gameboard.set_content(x, y, digit)
                            state = self.check_validity(test_gameboard)
                            if state > 0:
                                self.display_worker_text("")
                                self.add_result_text("Guess Test: Winner {obj} can remove {content} at {at}",
                                                     content, digit, x, y, digit)
                                digit += "w"
                                if digit in self.insert_guess:
                                    self.insert_guess[digit].append((x, y))
                                else:
                                    self.insert_guess[digit] = [(x, y)]
                                self.display_worker_text("", True)
                                return
                            if state < 0:
                                if digit in self.insert_guess:
                                    self.insert_guess[digit].append((x, y))
                                else:
                                    self.insert_guess[digit] = [(x, y)]
                                self.add_result_text("Guess Test: Looser {obj} can be removed at {at}",
                                                     content, digit, x, y, digit)
                                candidates_found = True
                            wx.GetApp().Yield()
            if candidates_found:
                break
        self.display_worker_text("", True)

    def calc_solverstring(self, event):
        content = self.real_gameboard.get_gameboard_string()

        url = f"https://www.str8ts.com/str8ts.htm?bd={content}"
        webbrowser.open(url)
        # wx.MessageBox(f"{content}{mask}", "Ready", style=wx.ICON_INFORMATION)


def main():
    app = wx.App()
    frame = Str8tsSolver(parent=None)
    frame.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
