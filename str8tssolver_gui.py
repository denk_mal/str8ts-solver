#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=R0912,R0913

"""Subclass of MainFrame, which translate standard functions to the gui framework (wxPython)."""
from typing import Dict, Union

import wx

import gameboard
import main_frame


class Str8tsSolverGUI(main_frame.MainFrame):
    DIGITS = "123456789"

    def __init__(self, parent):
        super().__init__(parent)
        self.real_gameboard = gameboard.Gameboard(self)
        self._content_changed = False
        self._highest_step = 0
        self._result_text = "Result"
        self.ctrl_down = False
        self.alt_down = False
        self._config: Dict[str, Union[int, bool, str]] = {
            'gamefile': '',
            'autorun': False,
            'autorun_delay': 500,
            'autorun_file': False,
            'stop_on_success': False,
            'stop_on_failure': True
        }

    def init_field(self):
        self._result_text = ""
        self._highest_step = 0
        self.m_resultstep.SetLabel("")

    def _set_result_label(self, idx, text, color=None):
        resultsizer = self.m_count_solved.GetContainingSizer()
        children = resultsizer.GetChildren()
        idx = (idx * 2) + 3
        if idx < len(children):
            result_label = children[idx].GetWindow()
            result_label.SetLabel(text)
            if color:
                result_label.SetForegroundColour(color)

    def clear_result_labels(self):
        self.m_count_solved.SetLabel("")
        main_sizer = self.m_count_solved.GetContainingSizer()
        list_length = len(main_sizer.GetChildren()) // 2
        for idx in range(list_length - 1):
            self._set_result_label(idx, "")

    def set_result_label(self, idx, is_ok):
        if is_ok:
            self._set_result_label(idx, "YES", wx.Colour('GREEN'))
        else:
            self._set_result_label(idx, "NO", wx.Colour('RED'))

    def set_result_text(self, new_text=""):
        self.m_result.SetValue(new_text)
        self._result_text = new_text

    def add_result_text(self, prefix, old_content, new_content, x, y, obj):
        if old_content == new_content:
            return
        content = ""
        for char in old_content:
            if char not in new_content:
                content += char + ","
        at = gameboard.Gameboard.convert_coords_to_gui(x, y)
        new_text = prefix.format(obj=obj, at=at, content=content[:-1])
        self._result_text += new_text + "\n"
        self.m_result.SetValue(self._result_text)

    def set_label_text(self, x, y, label_text, is_error, solution_stage):
        pos = x - 1 + ((y - 1) * 9)
        children = self.m_gameboard.GetChildren()
        panel = children[pos]
        panel.set_label_text(label_text, is_error, solution_stage)

    def set_resultstep(self, idx: int):
        if idx <= self._highest_step:
            return
        self._highest_step = idx
        resultsizer = self.m_count_solved.GetContainingSizer()
        children = resultsizer.GetChildren()
        step_text = children[idx * 2].Window.GetLabel()
        self.m_resultstep.SetLabel(step_text)
        self.Layout()

    def create_new_game(self, event):
        dlg = CreateDialogGUI(self)
        if dlg:
            content = self.real_gameboard.get_gameboard_string()
            dlg.init_field(content[:81], content[81:])
            dlg.ShowModal()
            content = dlg.get_gameboard_string()
            if content:
                self.real_gameboard.init_field(content[:81], content[81:])
                self.clear_result_labels()

class DigitPanelGui(main_frame.DigitPanel):
    def __init__(self, parent, mode, x, y):
        super().__init__(parent)
        self.x = x
        self.y = y
        self.isBlock = False
        self.isSolution = False
        self.Digit.Hide()
        self.Digits.Hide()
        div, modulo = divmod(mode, 10)
        if div:
            if modulo:
                self.Digit.Show()
                self.m_digit_text.SetForegroundColour(wx.Colour('BLACK'))
                self.m_digit_text.SetBackgroundColour(wx.Colour('WHITE'))
                self.m_digit_text.SetLabel(f"{mode % 10}")
                self.isSolution = True
            else:
                self.Digits.Show()
                self.m_digit_text.SetLabel(Str8tsSolverGUI.DIGITS)
        else:
            self.isBlock = True
            self.Digit.Show()
            self.m_digit_text.SetForegroundColour(wx.Colour('WHITE'))
            self.m_digit_text.SetBackgroundColour(wx.Colour('BLACK'))
            if modulo:
                self.m_digit_text.SetLabel(f"{mode % 10}")

    def set_label_text(self, labeltext, is_error, solution_stage):
        if self.isBlock:
            return

        if is_error:
            if self.isSolution:
                self.m_digit_text.SetForegroundColour(wx.Colour('BLACK'))
            else:
                self.m_digit_text.SetForegroundColour(wx.Colour('SKY BLUE'))
            self.m_digit_text.SetBackgroundColour(wx.Colour('RED'))
        elif solution_stage > 0:
            self.m_digit_text.SetForegroundColour(wx.Colour('SKY BLUE'))
            children = self.Digits.GetChildren()
            for digit in self.Digits.GetChildren():
                if len(labeltext) == 1:
                    digit.SetBackgroundColour(wx.Colour('SPRING GREEN'))
                idx = 0
                for smalldigit in Str8tsSolverGUI.DIGITS:
                    old_labeltext = children[idx].GetLabel()
                    if smalldigit in labeltext:
                        children[idx].SetLabel(smalldigit)
                    else:
                        if solution_stage > 2 and len(labeltext) != 1 and smalldigit in old_labeltext:
                            children[idx].SetBackgroundColour(wx.Colour('YELLOW'))
                            children[idx].SetForegroundColour(wx.Colour('RED'))
                            children[idx].SetLabel(smalldigit)
                        else:
                            children[idx].SetLabel("")
                    idx += 1
        else:
            if len(labeltext) > 1:
                self.Digit.Hide()
                self.Digits.Show()
                children = self.Digits.GetChildren()
                idx = 0
                for digit in Str8tsSolverGUI.DIGITS:
                    if digit in labeltext:
                        children[idx].SetLabel(digit)
                    else:
                        children[idx].SetLabel("")
                    idx += 1
                for digit in self.Digits.GetChildren():
                    digit.SetForegroundColour(wx.Colour('BLACK'))
                    digit.SetBackgroundColour(wx.Colour('WHITE'))
            else:
                # self.isSolution = True
                self.Digit.Show()
                self.Digits.Hide()
        self.m_digit_text.SetLabel(labeltext)

    def on_key_edit(self, event):
        if self.isBlock or self.isSolution:
            return
        with TextInput(self, self.m_digit_text.GetLabel()) as dlg:
            dlg.ShowModal()
            newvalue = dlg.m_text_input.GetValue()
            self.set_label_text(newvalue, False, 1)
            top_frame = wx.GetTopLevelParent(self)
            top_frame.real_gameboard.set_content(self.x, self.y, newvalue)

class ConfigDialogGUI(main_frame.ConfigDialog):
    def __init__(self, parent, config):
        super().__init__(parent)
        try:
            self.m_file_picker.SetPath(config['gamefile'])
            self.m_autorun_single.SetValue(config['autorun'])
            self.m_panel_autorun_data.Enable(config['autorun'])
            self.m_slider_delay.SetValue(config['autorun_delay'])
            self.m_autorun_file.SetValue(config['autorun_file'])
            self.m_stop_on_success.SetValue(config['stop_on_success'])
            self.m_stop_on_failure.SetValue(config['stop_on_failure'])
            self.m_singlestep.SetValue(config['singlestep'])
        except KeyError:
            pass  # ignore missing settings from config file (will be default)
        self.m_slider_delay.SetLineSize(100)
        self.m_slider_delay.SetPageSize(250)

    def save_config(self, event):
        self.EndModal(wx.ID_SAVE)

    def set_autorun_panel(self, event):
        self.m_panel_autorun_data.Enable(self.m_autorun_single.GetValue())

class TextInput(main_frame.TextInput):
    # pylint: disable=R0903
    def __init__(self, parent, inittext):
        super().__init__(parent)
        self.m_text_input.SetValue(inittext)

    def edit_ready(self, event):
        self.Close()

class CreateDialogGUI(main_frame.CreateDialog):
    def __init__(self, parent):
        super().__init__(parent)
        sizer = self.m_gameboard.GetSizer()
        for _x in range(81):
            new_field = DigitInputGUI(self.m_gameboard)
            sizer.Add(new_field, 1, wx.ALL | wx.EXPAND, 1)
        self.SendSizeEventToParent()

    def get_gameboard_string(self):
        sizer = self.m_gameboard.GetSizer()
        children = sizer.GetChildren()
        content = ""
        mask = ""
        for child in children:
            widget = child.GetWindow()
            digit, mode = widget.get_digit()
            content += digit
            mask += mode
        return content + mask

    def init_field(self, content: str, mask: str) -> bool:
        sizer = self.m_gameboard.GetSizer()
        children = sizer.GetChildren()
        for idx, digit in enumerate(content):
            mode = mask[idx]
            child = children[idx]
            widget = child.GetWindow()
            widget.set_digit(digit, mode)
        self.SendSizeEventToParent()

    def clean_field(self, event):
        self.init_field("0"*81, "0"*81)

class DigitInputGUI(main_frame.DigitInput):
    def __init__(self, parent):
        super().__init__(parent)
        self.content = 10

    def get_digit(self):
        digit = abs(self.content) % 10
        if self.content > 0:
            mode = "0"
        else:
            mode = "1"
        return str(digit), mode

    def set_digit(self, digit, mode):
        digit_int = int(digit)
        if mode == "1":
            digit_int *= -1
        elif digit_int == 0:
            digit_int = 10
        self.content = digit_int
        self.display_digit()

    def display_digit(self):
        if self.content > 0:
            self.m_digit_text.SetForegroundColour(wx.BLACK)
            self.m_digit_text.SetBackgroundColour(wx.WHITE)
        else:
            self.m_digit_text.SetForegroundColour(wx.WHITE)
            self.m_digit_text.SetBackgroundColour(wx.BLACK)
        digit = abs(self.content)
        if 0 < digit < 10:
            self.m_digit_text.SetLabel(str(digit))
        else:
            self.m_digit_text.SetLabel("")

    def on_left_up(self, event):
        if self.content == 10:
            self.content = 0
        elif self.content == 0:
            self.content = 10
        else:
            self.content *= -1
        self.display_digit()

    def on_right_down(self, event):
        menu = wx.Menu()
        for digit in "0123456789":
            menuitem = wx.MenuItem(menu, wx.NewId(), digit)
            menu.Bind(wx.EVT_MENU, self.select_menu, menuitem)
            menu.Append(menuitem)
        self.PopupMenu(menu, event.GetPosition())

    def select_menu(self, event):
        id_selected = event.GetId()
        menu = event.GetEventObject()
        digit = int(menu.GetLabel(id_selected))
        if digit:
            if self.content > 0:
                self.content = digit
            else:
                self.content = digit * -1
        else:
            if self.content > 0:
                self.content = 10
            else:
                self.content = 0
        self.display_digit()

if __name__ == '__main__':
    import str8tssolver
    str8tssolver.main()
